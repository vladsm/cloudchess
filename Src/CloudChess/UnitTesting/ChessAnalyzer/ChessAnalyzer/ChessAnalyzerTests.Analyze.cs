﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;

using CloudChess.Analysis;
using CloudChess.Analysis.Uci;

using Moq;

using NUnit.Framework;

namespace CloudChess.UnitTesting
{
	public sealed partial class ChessAnalyzerTests
	{
		/// <summary>
		/// Checks that Position method...
		/// </summary>
		[TestFixture]
		[Category("ChessAnalyzer/ChessAnalyzerTests")]
		public sealed class TheAnalyzeMethod : ChessAnalyzerMethodTestsBase
		{
			/// <summary>
			/// Sends "go" command to UCI engine.
			/// </summary>
			[Test]
			public void SendsGoCommandToUciEngine()
			{
				UciEngineMock uciEngineMock;
				var analyzer = CreateAndRunMockedChessAnalyzer(out uciEngineMock);

				var analysisParameters = new PositionAnalysisParameters();
				var analyzeProgress = analyzer.Analyze(analysisParameters);
				uciEngineMock.Verify(engine => engine.SendCommand(UciCommands.Go(analysisParameters)), Times.Never());

				analyzeProgress.Subscribe();
				uciEngineMock.Verify(engine => engine.SendCommand(UciCommands.Go(analysisParameters)), Times.Once());
			}

			/// <summary>
			/// Doesn't stop returning traces while UCI-engine doesn't send "bestmove" response.
			/// </summary>
			[Test]
			public void DoesntStopReturningTracesWhileUciEngineDoesntSendBestMoveResponse()
			{
				var analysisParameters = new PositionAnalysisParameters();
				var analyzer = CreateAndRunMockedChessAnalyzer(uciEngineMock => 
					uciEngineMock.
						Setup(engine => engine.SendCommand(UciCommands.Go(analysisParameters))).
						Callback(() =>
							{
								uciEngineMock.ResponsesMessages.Add<UciResponse>(OnNext(10, (UciResponse)new InfoUciResponse()));
								uciEngineMock.ResponsesMessages.Add<UciResponse>(OnNext(1000, (UciResponse)new InfoUciResponse()));
								uciEngineMock.ResponsesMessages.Add<UciResponse>(OnNext(10000, (UciResponse)new InfoUciResponse()));
							})
						);

				bool analysisCompleted = false;
				analyzer.Analyze(analysisParameters).Subscribe(_ => { }, () => { analysisCompleted = true; });
				Scheduler.Start();

				Assert.That(analysisCompleted, Is.False);
			}

			/// <summary>
			/// Stops returning traces when UCI-engine sends "bestmove" response.
			/// </summary>
			[Test]
			public void StopsReturningTracesWhenUciEngineSendsBestMoveResponse()
			{
				var analysisParameters = new PositionAnalysisParameters();
				var analyzer = CreateAndRunMockedChessAnalyzer(uciEngineMock =>
					uciEngineMock.
						Setup(engine => engine.SendCommand(UciCommands.Go(analysisParameters))).
						Callback(() =>
						{
							uciEngineMock.ResponsesMessages.Add<UciResponse>(OnNext(10, (UciResponse)new InfoUciResponse()));
							uciEngineMock.ResponsesMessages.Add<UciResponse>(OnNext(1000, (UciResponse)new InfoUciResponse()));
							uciEngineMock.ResponsesMessages.Add<UciResponse>(
								OnNext(10000, (UciResponse)new BestMoveUciResponse(new CnMove("e2e4")))
								);
							uciEngineMock.ResponsesMessages.Add<UciResponse>(
								OnNext(10001, (UciResponse)new BestMoveUciResponse(new CnMove("d2d4")))
								);
						})
						);

				var analysisTraces = analyzer.Analyze(analysisParameters);
				
				bool analysisCompleted = false;
				var traces = new List<AnalysisTrace>();
				analysisTraces.TakeLast(1).Subscribe(traces.Add, () => { analysisCompleted = true; });
				
				var bestMoveTraces = new List<BestMoveAnalysisTrace>();
				analysisTraces.OfType<BestMoveAnalysisTrace>().Subscribe(bestMoveTraces.Add);

				Scheduler.Start();

				Assert.That(analysisCompleted, Is.True);
				
				Assert.That(traces.Count, Is.EqualTo(1));
				Assert.That(traces[0], Is.InstanceOf<BestMoveAnalysisTrace>());

				Assert.That(bestMoveTraces.Count, Is.EqualTo(1));
			}

			[Test]
			[TestCaseSource("MapsResponsesFromUciEngineAnalysisTestSuite")]
			public IEnumerable<AnalysisTrace> MapsResponsesFromUciEngineAnalysis(UciResponse uciResponse)
			{
				var analysisParameters = new PositionAnalysisParameters();
				var analyzer = CreateAndRunMockedChessAnalyzer(uciEngineMock =>
					uciEngineMock.
						Setup(engine => engine.SendCommand(UciCommands.Go(analysisParameters))).
						Callback(() =>
						{
							if (uciResponse != null)
							{
								uciEngineMock.ResponsesMessages.Add<UciResponse>(OnNext(100, uciResponse));
							}
							uciEngineMock.ResponsesMessages.Add<UciResponse>(
								OnNext(10000, (UciResponse)new BestMoveUciResponse(new CnMove("e2e4"), new CnMove("e7e5")))
								);
						})
						);

				var traces = new List<Timestamped<AnalysisTrace>>();
				analyzer.Analyze(analysisParameters).Timestamp(Scheduler).Subscribe(traces.Add);
				var analysisStartedTicks = Scheduler.Clock;
				Scheduler.Start();

				if (uciResponse == null)
				{
					Assert.That(traces.Count, Is.EqualTo(1));
					var bestMoveTraceTimestamped = traces.Last();
					var bestMoveTrace = (BestMoveAnalysisTrace)bestMoveTraceTimestamped.Value;
					Assert.That(bestMoveTraceTimestamped.Timestamp.Ticks, Is.EqualTo(analysisStartedTicks + 10000));
					Assert.That(bestMoveTrace.BestMove, Is.EqualTo(new CnMove("e2e4")));
					Assert.That(bestMoveTrace.Ponder, Is.EqualTo(new CnMove("e7e5")));
				}

				traces.RemoveAt(traces.Count - 1);
				Assert.That(
					traces.Select(t => t.Timestamp.Ticks).ToArray(),
					Is.EquivalentTo(Enumerable.Repeat(analysisStartedTicks + 100, traces.Count))
					);
				
				return traces.Select(t => t.Value).ToArray();
			}

			public static IEnumerable MapsResponsesFromUciEngineAnalysisTestSuite
			{
				get
				{
					yield return new TestCaseData(null).
						Returns(Enumerable.Empty<AnalysisTrace>()).
						SetName("BestMove");
					
					// Progress trace

					yield return new TestCaseData(new InfoUciResponse { Time = 2000, Nodes = 3400000, NodesPerSecond = 1700000 }).
						Returns(new AnalysisTrace[] {new ProgressAnalysisTrace(2000, 3400000, 1700000, null, null, null, null)}).
						SetName("Progress trace <- time, nodes, nps");
					yield return new TestCaseData(new InfoUciResponse { Time = 2000, Nodes = 3400000 }).
						Returns(new AnalysisTrace[] { new ProgressAnalysisTrace(2000, 3400000, 1700000, null, null, null, null) }).
						SetName("Progress trace <- time, nodes");
					yield return new TestCaseData(new InfoUciResponse { Time = 2000, Nodes = 3400000, NodesPerSecond = 1700, Depth = 10, HashFull = 0, TbHits = 2, CpuLoad = 800 }).
						Returns(new AnalysisTrace[] { new ProgressAnalysisTrace(2000, 3400000, 1700, 10, 0, 2, 800) }).
						SetName("Progress trace <- time, nodes, nps, depth, hashfull, tbhits, cpuload");
					yield return new TestCaseData(
						new InfoUciResponse
						{
							Pv = new[] { new CnMove("e2e4"), new CnMove("d2d4") },
							Time = 2000,
							Nodes = 3000400,
							NodesPerSecond = 1500200,
							Depth = 18,
							SelectiveDepth = 23,
							HashFull = 30,
							TbHits = 14
						}
						).
						Returns(new AnalysisTrace[] { new ProgressAnalysisTrace(2000, 3000400, 1500200, 18, 30, 14, null) }).
						SetName("Pprogress trace <- pv, nodes, nps, depth, seldepth, hashful, tbhits");
					yield return new TestCaseData(
						new InfoUciResponse
						{
							PositionEstimation = new PositionScore(12),
							Time = 2000,
							Nodes = 3000400,
							NodesPerSecond = 1500200,
							Depth = 18,
							SelectiveDepth = 23,
							HashFull = 30,
							TbHits = 14
						}
						).
						Returns(new AnalysisTrace[] { new ProgressAnalysisTrace(2000, 3000400, 1500200, 18, 30, 14, null) }).
						SetName("Progress trace <- score, nodes, nps, depth, seldepth, hashful, tbhits");

					// Current move trace

					yield return new TestCaseData(new InfoUciResponse { CurrentMove = new CnMove("e2e4"), CurrentMoveNumber = 1 }).
						Returns(new AnalysisTrace[] { new CurrentMoveAnalysisTrace(new CnMove("e2e4"), 1) }).
						SetName("Current move trace <- currmove, currmovenumber");
					yield return new TestCaseData(new InfoUciResponse { CurrentMove = new CnMove("e2e4"), CurrentMoveNumber = 1, Time = 2000}).
						Returns(new AnalysisTrace[] { new CurrentMoveAnalysisTrace(new CnMove("e2e4"), 1) }).
						SetName("Current move trace <- currmove, currmovenumber, time");
					yield return new TestCaseData(new InfoUciResponse { CurrentMove = new CnMove("e2e4"), CurrentMoveNumber = 1, Time = 2000, Nodes = 3400200}).
						Returns(new AnalysisTrace[]
						{
							new CurrentMoveAnalysisTrace(new CnMove("e2e4"), 1),
							new ProgressAnalysisTrace(2000, 3400200, 1700100, null, null, null, null)
						}).
						SetName("Current move trace + progress trace <- currmove, currmovenumber, time, nodes");

					// Current line trace

					yield return new TestCaseData(new InfoUciResponse { CurrentLine = new CurrentCalculatingLine(2, new[] {new CnMove("e2e4"), new CnMove("e7e5") })}).
						Returns(new AnalysisTrace[] { new CurrentLineAnalysisTrace(new CurrentCalculatingLine(2, new[] { new CnMove("e2e4"), new CnMove("e7e5") })) }).
						SetName("Current line trace <- currline");
					yield return new TestCaseData(new InfoUciResponse { CurrentLine = new CurrentCalculatingLine(2, new[] { new CnMove("e2e4"), new CnMove("e7e5") }), CurrentMove = new CnMove("e2e4"), CurrentMoveNumber = 1 }).
						Returns(new AnalysisTrace[]
						{
							new CurrentLineAnalysisTrace(new CurrentCalculatingLine(2, new[] { new CnMove("e2e4"), new CnMove("e7e5") })),
							new CurrentMoveAnalysisTrace(new CnMove("e2e4"), 1)
						}).
						SetName("Current line trace + current move trace <- currline, currmove, currmovenumber");

					// Principal variation trace

					yield return new TestCaseData(
						new InfoUciResponse
						{
							Pv = new[] {new CnMove("e2e4"), new CnMove("d2d4") },
							MultiPv = 10,
							PositionEstimation = new PositionScore(15, PositionScoreType.LowerBound)
						}
						).
						Returns(new AnalysisTrace[]
						{
							new PvAnalysisTrace(
								new[] {new CnMove("e2e4"), new CnMove("d2d4") },
								10,
								new PositionScore(15, PositionScoreType.LowerBound),
								null, null, null, null, null, null, null
								)
						}).
						SetName("Pv trace <- pv, score, multipv");
					yield return new TestCaseData(
						new InfoUciResponse
						{
							Pv = new[] {new CnMove("e2e4"), new CnMove("d2d4") },
							PositionEstimation = new MatePrediction(-56),
							Time = 2000,
							Nodes = 3000400,
							NodesPerSecond = 1500200,
							Depth = 18,
							SelectiveDepth = 23,
							HashFull = 30,
							TbHits = 14
						}
						).
						Returns(new AnalysisTrace[]
						{
							new PvAnalysisTrace(
								new[] {new CnMove("e2e4"), new CnMove("d2d4") },
								1,
								new MatePrediction(-56), 2000, 3000400, 1500200, 18, 23, 30, 14
								)
						}).
						SetName("Pv trace <- pv, score, nodes, nps, depth, seldepth, hashful, tbhits");

					// Refutation trace

					yield return new TestCaseData(new InfoUciResponse { Refutation = new[] {new CnMove("e2e4"), new CnMove("c7c5") }}).
						Returns(new AnalysisTrace[] { new RefutationAnalysisTrace(new[] { new CnMove("e2e4"), new CnMove("c7c5") }) }).
						SetName("Refutation trace <- refutation");
					yield return new TestCaseData(new InfoUciResponse { Refutation = new[] { new CnMove("e2e4"), new CnMove("c7c5") }, CurrentMove = new CnMove("e2e4"), CurrentMoveNumber = 1 }).
						Returns(new AnalysisTrace[]
						{
							new CurrentMoveAnalysisTrace(new CnMove("e2e4"), 1),
							new RefutationAnalysisTrace(new[] { new CnMove("e2e4"), new CnMove("c7c5") })
						}).
						SetName("Current move trace + refutation trace <- refutation, currmove, currmovenumber");

					// Message trace

					yield return new TestCaseData(new InfoUciResponse { Message = "A message from the UCI engine"}).
						Returns(new AnalysisTrace[] { new MessageAnalysisTrace("A message from the UCI engine") }).
						SetName("Message trace <- string");

					// No trace

					yield return new TestCaseData(new InfoUciResponse { Nodes = 3400000 }).
						Returns(Enumerable.Empty<AnalysisTrace>()).
						SetName("No trace <- nodes");
					yield return new TestCaseData(new InfoUciResponse { Time = 2000 }).
						Returns(Enumerable.Empty<AnalysisTrace>()).
						SetName("No trace <- time");
					yield return new TestCaseData(new InfoUciResponse { CurrentMove = new CnMove("e2e4") }).
						Returns(Enumerable.Empty<AnalysisTrace>()).
						SetName("No trace <- currmove");
					yield return new TestCaseData(new InfoUciResponse { CurrentMoveNumber = 1}).
						Returns(Enumerable.Empty<AnalysisTrace>()).
						SetName("No trace <- currmovenumber");
				}
			}

		}
	}
}

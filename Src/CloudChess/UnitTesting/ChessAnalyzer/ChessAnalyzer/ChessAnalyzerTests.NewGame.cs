﻿using System;

using CloudChess.Analysis.Uci;

using Moq;

using NUnit.Framework;

namespace CloudChess.UnitTesting
{
	public sealed partial class ChessAnalyzerTests
	{
		/// <summary>
		/// Checks that NewGame method...
		/// </summary>
		[TestFixture]
		[Category("ChessAnalyzer/ChessAnalyzerTests")]
		public sealed class TheNewGameMethod : ChessAnalyzerMethodTestsBase
		{
			/// <summary>
			/// Sends "ucinewgame" command to UCI engine.
			/// </summary>
			[Test]
			public void SendsUciNewGameCommandToUciEngine()
			{
				UciEngineMock uciEngineMock;
				var analyzer = CreateAndRunMockedChessAnalyzer(out uciEngineMock);

				var newGameProgress = analyzer.NewGame();
				uciEngineMock.Verify(engine => engine.SendCommand(UciCommands.NewGame), Times.Never());

				newGameProgress.Subscribe();
				uciEngineMock.Verify(engine => engine.SendCommand(UciCommands.NewGame), Times.Once());
			}

			/// <summary>
			/// Returns empty observable immediately.
			/// </summary>
			[Test]
			public void ReturnsEmptyObservableImmediately()
			{
				var analyzer = CreateAndRunMockedChessAnalyzer(uciEngineMock =>
					uciEngineMock.
						Setup(engine => engine.SendCommand(UciCommands.NewGame)).
						Callback(() =>
							uciEngineMock.ResponsesMessages.Add(OnNext(100, (UciResponse)new ReadyOkUciResponse()))
							)
					);

				int responseCounter = 0;
				bool completed = false;
				analyzer.NewGame().Subscribe(_ => responseCounter++, () => completed = true);

				Assert.That(responseCounter, Is.EqualTo(0));
				Assert.That(completed, Is.True);
			}
		}
	}
}

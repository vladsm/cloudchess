﻿using System.Reactive;
using System.Threading.Tasks;

using CloudChess.Analysis.Uci;

using Moq;

using NUnit.Framework;

namespace CloudChess.UnitTesting
{
	public sealed partial class ChessAnalyzerTests
	{
		/// <summary>
		/// Checks that Close method...
		/// </summary>
		[TestFixture]
		[Category("ChessAnalyzer/ChessAnalyzerTests")]
		public sealed class TheCloseMethod : ChessAnalyzerMethodTestsBase
		{
			/// <summary>
			/// Sends "quit" command to UCI engine.
			/// </summary>
			[Test]
			public void SendsQuitCommandToUciEngine()
			{
				UciEngineMock uciEngineMock;
				var analyzer = CreateAndRunMockedChessAnalyzer(out uciEngineMock);
				
				analyzer.Close();
				Scheduler.Start();

				uciEngineMock.Verify(engine => engine.SendCommand(UciCommands.Quit), Times.Once());
			}

			/// <summary>
			/// Waits until UCI-engine closed.
			/// </summary>
			[Test]
			public void WaitsUntilUciEngineClosed()
			{
				var analyzer = CreateAndRunMockedChessAnalyzer(uciEngineMock =>
					uciEngineMock.
						Setup(engine => engine.SendCommand(UciCommands.Quit)).
						Callback(() => uciEngineMock.ClosedMessages.Add(OnNext(400, Unit.Default)))
					);

				var closeProgress = analyzer.Close();
				
				Scheduler.AdvanceBy(399);
				Assert.That(closeProgress.Status, Is.Not.EqualTo(TaskStatus.RanToCompletion));

				Scheduler.AdvanceBy(1);
				Assert.That(closeProgress.Status, Is.EqualTo(TaskStatus.RanToCompletion));
			}

			/// <summary>
			/// Disposes UCI engine.
			/// </summary>
			[Test]
			public void DisposesUciEngine()
			{
				UciEngineMock theUciEngineMock;
				var analyzer = CreateAndRunMockedChessAnalyzer(uciEngineMock =>
					uciEngineMock.
						Setup(engine => engine.SendCommand(UciCommands.Quit)).
						Callback(() => uciEngineMock.ClosedMessages.Add(OnNext(400, Unit.Default))),
					out theUciEngineMock
					);

				analyzer.Close();
				Scheduler.AdvanceBy(400);

				theUciEngineMock.Verify(engine => engine.Dispose(), Times.AtLeastOnce());
			}
		}
	}
}

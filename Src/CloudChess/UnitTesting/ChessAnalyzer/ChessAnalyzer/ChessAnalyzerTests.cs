﻿using System;

using CloudChess.Analysis;
using CloudChess.Analysis.Uci;

using Microsoft.Reactive.Testing;

using NUnit.Framework;

namespace CloudChess.UnitTesting
{
	public sealed partial class ChessAnalyzerTests
	{
		public abstract class ChessAnalyzerMethodTestsBase : ReactiveTestFixture
		{
			private ISchedulerProvider _schedulerProvider;

			[SetUp]
			public void SetUp()
			{
				_schedulerProvider = new TestSchedulerProvider();
			}

			internal TestScheduler Scheduler
			{
				get { return (TestScheduler)_schedulerProvider.Default; }
			}


			internal UciEngineMock GetUciEngineMock()
			{
				return new UciEngineMock(Scheduler);
			}

			internal ChessAnalyzer CreateMockedChessAnalyzer(
				Action<UciEngineMock> customizeUciEngineMock,
				out UciEngineMock usedUciEngineMock
				)
			{
				var uciEngineMock = GetUciEngineMock();
				customizeUciEngineMock(uciEngineMock);
				usedUciEngineMock = uciEngineMock;
				return new ChessAnalyzer(uciEngineMock.Object, _schedulerProvider);
			}

			internal ChessAnalyzer CreateMockedChessAnalyzer(Action<UciEngineMock> customizeUciEngineMock)
			{
				UciEngineMock usedUciEngineMock;
				return CreateMockedChessAnalyzer(customizeUciEngineMock, out usedUciEngineMock);
			}

			internal ChessAnalyzer CreateMockedChessAnalyzer(out UciEngineMock usedUciEngineMock)
			{
				return CreateMockedChessAnalyzer(_ => { }, out usedUciEngineMock);
			}

			internal ChessAnalyzer CreateAndRunMockedChessAnalyzer(
				Action<UciEngineMock> customizeUciEngineMock,
				out UciEngineMock usedUciEngineMock
				)
			{
				UciEngineMock uciEngineMock;
				var analyzer = CreateMockedChessAnalyzer(customizeUciEngineMock, out uciEngineMock);
				uciEngineMock.
					Setup(engine => engine.SendCommand(UciCommands.InitUci)).
					Callback(() => uciEngineMock.ResponsesMessages.Add(OnNext(10, (UciResponse)new UciOkResponse())));

				var runProgress = analyzer.Run();
				Scheduler.AdvanceTo(11);
				runProgress.Wait();

				usedUciEngineMock = uciEngineMock;
				return analyzer;
			}

			internal ChessAnalyzer CreateAndRunMockedChessAnalyzer(Action<UciEngineMock> customizeUciEngineMock)
			{
				UciEngineMock usedUciEngineMock;
				return CreateAndRunMockedChessAnalyzer(customizeUciEngineMock, out usedUciEngineMock);
			}

			internal ChessAnalyzer CreateAndRunMockedChessAnalyzer(out UciEngineMock usedUciEngineMock)
			{
				return CreateAndRunMockedChessAnalyzer(_ => { }, out usedUciEngineMock);
			}
		}
	}
}

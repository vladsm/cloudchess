﻿using System;

using CloudChess.Analysis.Uci;

using Moq;

using NUnit.Framework;

namespace CloudChess.UnitTesting
{
	public sealed partial class ChessAnalyzerTests
	{
		/// <summary>
		/// Checks that IsReady method...
		/// </summary>
		[TestFixture]
		[Category("ChessAnalyzer/ChessAnalyzerTests")]
		public sealed class TheIsReadyMethod : ChessAnalyzerMethodTestsBase
		{
			/// <summary>
			/// Sends "isready" command to UCI engine.
			/// </summary>
			[Test]
			public void SendsIsReadyCommandToUciEngine()
			{
				UciEngineMock uciEngineMock;
				var analyzer = CreateAndRunMockedChessAnalyzer(out uciEngineMock);

				analyzer.IsReady().Subscribe();

				uciEngineMock.Verify(engine => engine.SendCommand(UciCommands.IsReady), Times.Once());
			}

			/// <summary>
			/// Waits for "readyok" response from UCI engine.
			/// </summary>
			[Test]
			public void WaitsForReadyOkResponseFromUciEngine()
			{
				var analyzer = CreateAndRunMockedChessAnalyzer(uciEngineMock =>
					uciEngineMock.
						Setup(engine => engine.SendCommand(UciCommands.IsReady)).
						Callback(() =>
							uciEngineMock.ResponsesMessages.Add(OnNext(100, (UciResponse)new ReadyOkUciResponse()))
							)
					);
				int readyCounter = 0;
				bool completed = false;
				analyzer.IsReady().Subscribe(_ => readyCounter++, () => completed = true);

				Scheduler.AdvanceBy(99);
				Assert.That(readyCounter, Is.EqualTo(0));
				Assert.That(completed, Is.False);

				Scheduler.AdvanceBy(1);
				Assert.That(readyCounter, Is.EqualTo(1));
				Assert.That(completed, Is.True);
			}
		}
	}
}

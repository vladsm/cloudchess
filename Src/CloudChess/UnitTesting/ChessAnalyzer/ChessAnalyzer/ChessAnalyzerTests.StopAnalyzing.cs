﻿using CloudChess.Analysis.Uci;

using Moq;

using NUnit.Framework;

namespace CloudChess.UnitTesting
{
	public sealed partial class ChessAnalyzerTests
	{
		/// <summary>
		/// Checks that StopAnalyzing method...
		/// </summary>
		[TestFixture]
		[Category("ChessAnalyzer/ChessAnalyzerTests")]
		public sealed class TheStopAnalyzingMethod : ChessAnalyzerMethodTestsBase
		{
			/// <summary>
			/// Sends "stop" command to UCI engine.
			/// </summary>
			[Test]
			public void SendsPositionCommandToUciEngine()
			{
				UciEngineMock uciEngineMock;
				var analyzer = CreateAndRunMockedChessAnalyzer(out uciEngineMock);

				analyzer.StopAnalyzing();
				uciEngineMock.Verify(engine => engine.SendCommand(UciCommands.Stop), Times.Once());
			}
		}
	}
}

﻿using System;
using System.Threading.Tasks;

using CloudChess.Analysis;
using CloudChess.Analysis.Uci;

using Moq;

using NUnit.Framework;

namespace CloudChess.UnitTesting
{
	public sealed partial class ChessAnalyzerTests
	{
		/// <summary>
		/// Checks that Run mehod...
		/// </summary>
		[TestFixture]
		[Category("ChessAnalyzer/ChessAnalyzerTests")]
		public sealed class TheRunMethod : ChessAnalyzerMethodTestsBase
		{
			/// <summary>
			/// Applies default settings.
			/// </summary>
			[Test]
			public void AppliesDefaultSettings()
			{
				var analyzer = CreateMockedChessAnalyzer(_ => { });

				Assert.That(analyzer.UciInitializationTimeout, Is.EqualTo(TimeSpan.FromSeconds(30)));
				Assert.That(analyzer.UciInitializationMaximumRetries, Is.EqualTo(3));
			}

			/// <summary>
			/// Runs UCI-engine.
			/// </summary>
			[Test]
			public void RunsUciEngine()
			{
				UciEngineMock uciEngineMock;
				var analyzer = CreateMockedChessAnalyzer(out uciEngineMock);

				analyzer.Run();

				uciEngineMock.Verify(e => e.Run(), Times.Once());
			}

			/// <summary>
			/// Throws <see cref="UciEngineLoadException"/> exception when UCI engine canot be loaded.
			/// </summary>
			[Test]
			public void ThrowsExceptionWhenUciEngineCannotBeLoaded()
			{
				var analyzer = CreateMockedChessAnalyzer(uciEngineMock =>
					uciEngineMock.Setup(e => e.Run()).Throws<UciEngineLoadException>()
					);

				Assert.That(
					() => analyzer.Run(),
					Throws.
						Exception.InstanceOf<ChessAnalyzerInitializationException>().And.
						InnerException.InstanceOf<UciEngineLoadException>()
					);
			}

			/// <summary>
			/// Sends "uci" command to engine.
			/// </summary>
			[Test]
			public void SendsUciCommandToEngine()
			{
				UciEngineMock uciEngineMock;
				var analyzer = CreateMockedChessAnalyzer(out uciEngineMock);

				analyzer.Run();

				uciEngineMock.Verify(e => e.SendCommand(UciCommands.InitUci), Times.Once());
			}

			/// <summary>
			/// Gets "uciok" response from engine on "uci" command.
			/// </summary>
			[Test]
			public void GetsUciokAsResponseFromEngineForUciCommand()
			{
				var analyzer = CreateMockedChessAnalyzer(uciEngineMock =>
					uciEngineMock.
						Setup(engine => engine.SendCommand(UciCommands.InitUci)).
						Callback(() => uciEngineMock.ResponsesMessages.Add(OnNext(400, (UciResponse)new UciOkResponse())))
					);

				bool runCompleted = false;
				var runProgress = analyzer.Run();
				var afterRunProgress =  runProgress.ContinueWith(_ => runCompleted = true);
				Scheduler.Start();
				afterRunProgress.Wait();

				Assert.That(runCompleted, Is.True);
				Assert.That(runProgress.Status, Is.EqualTo(TaskStatus.RanToCompletion));
			}

			/// <summary>
			/// Retries to send "uci" command when timeout.
			/// </summary>
			[Test]
			public void RetriesToSendUciCommandsWhenTimeout()
			{
				const int timeout = 20;

				UciEngineMock theUciEngineMock;
				var analyzer = CreateMockedChessAnalyzer(uciEngineMock =>
					uciEngineMock.
						Setup(engine => engine.SendCommand(UciCommands.InitUci)).
						CallbackInOrder(order =>
							{
								uciEngineMock.ResponsesMessages.Add(
									OnNext(TimeSpan.FromSeconds(3), (UciResponse)new OptionUciResponse())
									);
								// reply with "uciok" on third call
								if (order < 2) return;
								uciEngineMock.ResponsesMessages.Add(
									OnNext(TimeSpan.FromSeconds(timeout - 1), (UciResponse)new UciOkResponse())
									);
							}
							),
						out theUciEngineMock
					);
				analyzer.UciInitializationTimeout = TimeSpan.FromSeconds(timeout);

				bool runCompleted = false;
				var afterRunProgress = analyzer.Run().ContinueWith(_ => runCompleted = true);
				Scheduler.Start();
				afterRunProgress.Wait();

				theUciEngineMock.Verify(engine => engine.SendCommand(UciCommands.InitUci), Times.Exactly(3));
				Assert.That(runCompleted, Is.True);
			}

			/// <summary>
			/// Throws exception when retries number exceeds the maximum allowed while sending "uci" command.
			/// </summary>
			[Test]
			public void ThrowsExceptionWhenRetriesNumberExceedsTheMaximumAllowedWhileSendingUciCommand()
			{
				const int maximumRetries = 5;

				UciEngineMock theUciEngineMock;
				var analyzer = CreateMockedChessAnalyzer(uciEngineMock =>
					uciEngineMock.
						Setup(engine => engine.SendCommand(UciCommands.InitUci)).
						CallbackInOrder(order =>
							{
								// do not reply on first 4 calls
								if (order < maximumRetries) return;
								uciEngineMock.ResponsesMessages.Add(OnNext(400, (UciResponse)new UciOkResponse()));
							}
							),
						out theUciEngineMock
					);

				analyzer.UciInitializationMaximumRetries = maximumRetries;
				Task analyserRunProgress = analyzer.Run();
				Scheduler.Start();

				Assert.That(
					() => analyserRunProgress.Wait(),
					Throws.
						Exception.InstanceOf<AggregateException>().And.
						InnerException.InstanceOf<ChessAnalyzerInitializationException>().And.
						InnerException.Message.EqualTo("UCI engine timed out while UCI-mode initialization.")
					);
				theUciEngineMock.Verify(engine => engine.SendCommand(UciCommands.InitUci), Times.Exactly(maximumRetries));
			}

			/// <summary>
			/// Handles unknown errors while UCI initialization.
			/// </summary>
			[Test]
			public void HandlesUnknownErrorsWhileUciInitialization()
			{
				UciEngineMock theUciEngineMock;
				var analyzer = CreateMockedChessAnalyzer(uciEngineMock =>
					uciEngineMock.
						Setup(engine => engine.SendCommand(UciCommands.InitUci)).
						Callback(() => 
							uciEngineMock.ResponsesMessages.Add(
								OnError<UciResponse>(400, new OutOfMemoryException("No memory for hash."))
								)
							),
					out theUciEngineMock
					);

				Task analyserRunProgress = analyzer.Run();
				Scheduler.Start();

				Assert.That(
					() => analyserRunProgress.Wait(),
					Throws.
						Exception.InstanceOf<AggregateException>().And.
						InnerException.InstanceOf<ChessAnalyzerInitializationException>().And.
						InnerException.Message.EqualTo("No memory for hash.").And.
						InnerException.InnerException.InstanceOf<OutOfMemoryException>()
					);
			}
		}
	}
}

﻿using System;

using CloudChess.Analysis.Uci;

using Moq;

using NUnit.Framework;

namespace CloudChess.UnitTesting
{
	public sealed partial class ChessAnalyzerTests
	{
		/// <summary>
		/// Checks that SetPosition method...
		/// </summary>
		[TestFixture]
		[Category("ChessAnalyzer/ChessAnalyzerTests")]
		public sealed class TheSetPositionMethod : ChessAnalyzerMethodTestsBase
		{
			/// <summary>
			/// Sends "position" command to UCI engine.
			/// </summary>
			[Test]
			public void SendsPositionCommandToUciEngine()
			{
				UciEngineMock uciEngineMock;
				var analyzer = CreateAndRunMockedChessAnalyzer(out uciEngineMock);
				var positionMock = new Mock<IUciPosition>();
				positionMock.Setup(p => p.ToUciDescriptor()).Returns("startpos");
				var position = positionMock.Object;

				analyzer.SetPosition(position);
				uciEngineMock.Verify(engine => engine.SendCommand(UciCommands.Position(position)), Times.Once());
			}
		}
	}
}

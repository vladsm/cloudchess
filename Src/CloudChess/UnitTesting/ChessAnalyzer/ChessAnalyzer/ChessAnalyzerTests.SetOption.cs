﻿using CloudChess.Analysis.Uci;

using Moq;

using NUnit.Framework;

namespace CloudChess.UnitTesting
{
	public sealed partial class ChessAnalyzerTests
	{
		/// <summary>
		/// Checks that SetOption method...
		/// </summary>
		[TestFixture]
		[Category("ChessAnalyzer/ChessAnalyzerTests")]
		public sealed class TheSetOptionMethod : ChessAnalyzerMethodTestsBase
		{
			/// <summary>
			/// Sends "setoption" command to UCI engine.
			/// </summary>
			[Test]
			public void SendsSetOptionCommandToUciEngine()
			{
				UciEngineMock uciEngineMock;
				var analyzer = CreateAndRunMockedChessAnalyzer(out uciEngineMock);

				analyzer.SetOption("Option1", "Value1");
				uciEngineMock.Verify(engine => engine.SendCommand(UciCommands.SetOption("Option1", "Value1")), Times.Once());
			}
		}
	}
}

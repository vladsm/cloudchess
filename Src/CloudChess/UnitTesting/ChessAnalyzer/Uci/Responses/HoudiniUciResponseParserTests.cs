﻿using System.Collections;

using CloudChess.Analysis.Houdini;
using CloudChess.Analysis.Uci;

using NUnit.Framework;

namespace CloudChess.UnitTesting
{
	public sealed class HoudiniUciResponseParserTests
	{
		[TestFixture]
		[Category("ChessAnalyzer/Uci/HoudiniUciResponseParserTests")]
		public sealed class TheParseMethod
		{
			/// <summary>
			/// Parses UCI-response text to UciResponse object.
			/// </summary>
			[Test]
			[TestCaseSource("UciResponsesTestSuite")]
			public UciResponse ParsesUciResponseTextToIUciResponceObject(string uciResponseText)
			{
				return new UciResponseParser<HoudiniUciResponseGrammar>().Parse(uciResponseText);
			}

			public static IEnumerable UciResponsesTestSuite
			{
				get
				{
					yield return new TestCaseData("info cpuload 900 idle 456M").
						Returns(new InfoUciResponse {CpuLoad = 900}).
						SetName("info cpuload 900 idle 456M");
				}
			}
		}
	}
}

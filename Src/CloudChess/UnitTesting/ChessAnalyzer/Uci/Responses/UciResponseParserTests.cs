﻿using System.Collections;

using CloudChess.Analysis;
using CloudChess.Analysis.Uci;
using CloudChess.Analysis.Uci.Parsing;

using NUnit.Framework;

namespace CloudChess.UnitTesting
{
	public sealed class UciResponseParserTests
	{
		[TestFixture]
		[Category("ChessAnalyzer/Uci/UciResponseParserTests")]
		public sealed class TheParseMethod
		{
			/// <summary>
			/// Throws exception for unknown response type.
			/// </summary>
			[Test]
			public void ThrowsExceptionForUnknowResponseType()
			{
				var parser = new UciResponseParser<UciResponseGrammar>();

				Assert.That(
					() => parser.Parse("unknownuciresponse"),
					Throws.Exception.
						InstanceOf<UciResponseParsingException>().And.
						Message.EqualTo("Response string \"unknownuciresponse\" cannot be parsed.")
					);
			}

			/// <summary>
			/// Parses UCI-response text to UciResponse object.
			/// </summary>
			[Test]
			[TestCaseSource("UciResponsesTestSuite")]
			public UciResponse ParsesUciResponseTextToIUciResponceObject(string uciResponseText)
			{
				return new UciResponseParser<UciResponseGrammar>().Parse(uciResponseText);
			}

			public static IEnumerable UciResponsesTestSuite
			{
				get
				{
					// Statuses

					yield return new TestCaseData("uciok").Returns(new UciOkResponse()).SetName("uciok");
					yield return new TestCaseData("readyok").Returns(new ReadyOkUciResponse()).SetName("readyok");

					// Bestmove

					yield return new TestCaseData("bestmove e2e4").
						Returns(new BestMoveUciResponse(new CnMove("e2e4"))).
						SetName("bestmove e2e4");
					yield return new TestCaseData("bestmove e2e9").
						Throws(typeof(UciResponseParsingException)).
						SetName("bestmove e2e9 - Error");
					yield return new TestCaseData("bestmove e2e").
						Throws(typeof(UciResponseParsingException)).
						SetName("bestmove e2e - Error");
					yield return new TestCaseData("bestmove i2e4").
						Throws(typeof(UciResponseParsingException)).
						SetName("bestmove i2e4 - Error");
					yield return new TestCaseData("bestmove e2e4 ponder e7e5").
						Returns(new BestMoveUciResponse(new CnMove("e2e4"), new CnMove("e7e5"))).
						SetName("bestmove e2e4 ponder e7e5");
					yield return new TestCaseData("bestmove e7e8s").
						Throws(typeof(UciResponseParsingException)).
						SetName("bestmove e7e8s - Error");
					yield return new TestCaseData("bestmove e7e8q").
						Returns(new BestMoveUciResponse(new CnMove("e7e8q"))).
						SetName("bestmove e7e8q");
					yield return new TestCaseData("bestmove e7e8r").
						Returns(new BestMoveUciResponse(new CnMove("e7e8r"))).
						SetName("bestmove e7e8r");
					yield return new TestCaseData("bestmove e7e8b").
						Returns(new BestMoveUciResponse(new CnMove("e7e8b"))).
						SetName("bestmove e7e8b");
					yield return new TestCaseData("bestmove e7e8n").
						Returns(new BestMoveUciResponse(new CnMove("e7e8n"))).
						SetName("bestmove e7e8n");

					// Info

					yield return new TestCaseData("info depth 18").
						Returns(new InfoUciResponse {Depth = 18}).
						SetName("info depth 18");
					yield return new TestCaseData("info seldepth 11").
						Returns(new InfoUciResponse {SelectiveDepth = 11}).
						SetName("info seldepth 11");
					yield return new TestCaseData("info time 120000").
						Returns(new InfoUciResponse {Time = 120000}).
						SetName("info time 120000");
					yield return new TestCaseData("info nodes 32530208").
						Returns(new InfoUciResponse { Nodes = 32530208 }).
						SetName("info nodes 32530208");
					yield return new TestCaseData("info pv d2d4 g8f6 b1c3 d7d5").
						Returns(new InfoUciResponse { Pv = new[] {new CnMove("d2d4"), new CnMove("g8f6"), new CnMove("b1c3"), new CnMove("d7d5")} }).
						SetName("info pv d2d4 g8f6 b1c3 d7d5");
					yield return new TestCaseData("info multipv 30").
						Returns(new InfoUciResponse { MultiPv = 30 }).
						SetName("info multipv 30");
					yield return new TestCaseData("info currmove e2e4").
						Returns(new InfoUciResponse { CurrentMove = new CnMove("e2e4") }).
						SetName("info currmove e2e4");
					yield return new TestCaseData("info currmovenumber 2").
						Returns(new InfoUciResponse { CurrentMoveNumber = 2 }).
						SetName("info currmovenumber 2");
					yield return new TestCaseData("info hashfull 349").
						Returns(new InfoUciResponse { HashFull = 349 }).
						SetName("info hashfull 349");
					yield return new TestCaseData("info nps 8450").
						Returns(new InfoUciResponse { NodesPerSecond = 8450 }).
						SetName("info nps 8450");
					yield return new TestCaseData("info tbhits 0").
						Returns(new InfoUciResponse { TbHits = 0 }).
						SetName("info tbhits 0");
					yield return new TestCaseData("info cpuload 947").
						Returns(new InfoUciResponse { CpuLoad = 947 }).
						SetName("info cpuload 947");
					yield return new TestCaseData("info score cp 12").
						Returns(new InfoUciResponse { PositionEstimation = new PositionScore(12)}).
						SetName("info score cp 12");
					yield return new TestCaseData("info score cp -132").
						Returns(new InfoUciResponse { PositionEstimation = new PositionScore(-132)}).
						SetName("info score cp -132");
					yield return new TestCaseData("info score cp 12 lowerbound").
						Returns(new InfoUciResponse { PositionEstimation = new PositionScore(12, PositionScoreType.LowerBound)}).
						SetName("info score cp 12 lowerbound");
					yield return new TestCaseData("info score cp 200 upperbound").
						Returns(new InfoUciResponse { PositionEstimation = new PositionScore(200, PositionScoreType.UpperBound)}).
						SetName("info score cp 200 upperbound");
					yield return new TestCaseData("info score mate 3").
						Returns(new InfoUciResponse { PositionEstimation = new MatePrediction(3) }).
						SetName("info score mate 3");
					yield return new TestCaseData("info score mate -9").
						Returns(new InfoUciResponse { PositionEstimation = new MatePrediction(-9) }).
						SetName("info score mate -9");
					yield return new TestCaseData("info string The message from the UCI engine").
						Returns(new InfoUciResponse { Message = "The message from the UCI engine" }).
						SetName("info string The message from the UCI engine");
					yield return new TestCaseData("info string The message from the UCI engine cpuload 900").
						Returns(new InfoUciResponse { Message = "The message from the UCI engine cpuload 900" }).
						SetName("info string The message from the UCI engine cpuload 900");
					yield return new TestCaseData("info refutation d1h5 g6h5 a2a7").
						Returns(new InfoUciResponse { Refutation = new[] {new CnMove("d1h5"), new CnMove("g6h5"), new CnMove("a2a7") }}).
						SetName("info refutation d1h5 g6h5 a2a7");
					yield return new TestCaseData("info currline 3 d1h5 g6h5 a2a7").
						Returns(new InfoUciResponse { CurrentLine = new CurrentCalculatingLine(3, new[] {new CnMove("d1h5"), new CnMove("g6h5"), new CnMove("a2a7") })}).
						SetName("info currline 3 d1h5 g6h5 a2a7");
					yield return new TestCaseData("info currline d1h5 g6h5 a2a7").
						Returns(new InfoUciResponse { CurrentLine = new CurrentCalculatingLine(new[] {new CnMove("d1h5"), new CnMove("g6h5"), new CnMove("a2a7") })}).
						SetName("info currline d1h5 g6h5 a2a7");

					// Info - combined
					yield return new TestCaseData("info multipv 1 depth 16 seldepth 35 score cp 19 time 2517 nodes 4653104 nps 1848000 tbhits 0 hashfull 271 pv e2e4 e7e6 d2d4").
						Returns(
							new InfoUciResponse
							{
								MultiPv = 1,
								Depth = 16,
								SelectiveDepth = 35,
								PositionEstimation = new PositionScore(19),
								Time = 2517,
								Nodes = 4653104,
								NodesPerSecond = 1848000,
								TbHits = 0,
								HashFull = 271,
								Pv = new[] { new CnMove("e2e4"), new CnMove("e7e6"), new CnMove("d2d4") }
							}).
						SetName("info multipv 1 depth 16 seldepth 35 score cp 19 time 2517 nodes 4653104 nps 1848000 tbhits 0 hashfull 271 pv e2e4 e7e6 d2d4");
					yield return new TestCaseData("info currmove b1a3 currmovenumber 8").
						Returns(
							new InfoUciResponse
							{
								CurrentMove = new CnMove("b1a3"),
								CurrentMoveNumber = 8
							}).
						SetName("info currmove b1a3 currmovenumber 8");
				}
			}
		}
	}
}

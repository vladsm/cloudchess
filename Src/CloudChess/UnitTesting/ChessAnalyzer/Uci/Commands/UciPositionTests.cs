﻿using System.Collections;

using CloudChess.Analysis.Uci;

using NUnit.Framework;

namespace CloudChess.UnitTesting
{
	public sealed class UciPositionTests
	{
		/// <summary>
		/// Checks that ToUciDescriptor method...
		/// </summary>
		[TestFixture]
		[Category("ChessAnalyzer/Uci/UciPositionTests")]
		public sealed class TheToUciDescriptorMethod
		{
			/// <summary>
			///  Gets correct position descriptor.
			/// </summary>
			/// <param name="position">A UCI-position.</param>
			/// <returns>Position descriptor.</returns>
			[Test]
			[TestCaseSource("PositionsTestSuite")]
			public string GetsCorrectPositionDescriptor(IUciPosition position)
			{
				return position.ToUciDescriptor();
			}

			public static IEnumerable PositionsTestSuite
			{
				get
				{
					yield return new TestCaseData(new StartUciPosition()).Returns("startpos");
					yield return new TestCaseData(
						new StartUciPosition(new[] {new CnMove("e2e4 e7e5 g1f3")})
						).
						Returns("startpos moves e2e4 e7e5 g1f3");
					yield return new TestCaseData(
						new FenUciPosition("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1")
						).
						Returns("fen rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
					yield return new TestCaseData(
						new FenUciPosition(
							"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1",
							new[] { new CnMove("e2e4 e7e5 g1f3") }
							)
						).
						Returns("fen rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1 moves e2e4 e7e5 g1f3");
				}
			}
		}
	}
}

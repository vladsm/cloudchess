using System.Collections;

using CloudChess.Analysis.Uci;

using NUnit.Framework;

namespace CloudChess.UnitTesting
{
	public sealed class PositionAnalysisParametersTests
	{
		/// <summary>
		/// Checks that ToUciDescriptor method...
		/// </summary>
		[TestFixture]
		[Category("ChessAnalyzer/Uci/PositionAnalysisParametersTests")]
		public sealed class TheToUciDescriptorMethod
		{
			/// <summary>
			///  Gets correct analysis parameters descriptor.
			/// </summary>
			/// <param name="analysisParameters">An analysis parameters.</param>
			/// <returns>Analasis parameters descriptor.</returns>
			[Test]
			[TestCaseSource("AnalysisParametersTestSuite")]
			public string GetsCorrectAnalysisParametersDescriptor(PositionAnalysisParameters analysisParameters)
			{
				return analysisParameters.ToUciDescriptor();
			}

			public static IEnumerable AnalysisParametersTestSuite
			{
				get
				{
					yield return new TestCaseData(new PositionAnalysisParameters()).
						Returns(string.Empty).
						SetName("Empty");

					yield return new TestCaseData(
						new PositionAnalysisParameters
						{
							SearchMoves = new[] {new CnMove("e2e4"), new CnMove("d2d4")}
						}
						).
						Returns("searchmoves e2e4 d2d4").
						SetName("SearchMoves");

					yield return new TestCaseData(
						new PositionAnalysisParameters { MaxDepth = 18 }
						).
						Returns("depth 18").
						SetName("MaxDepth");

					yield return new TestCaseData(
						new PositionAnalysisParameters { MaxNodes = 3000000 }
						).
						Returns("nodes 3000000").
						SetName("MaxNodes");

					yield return new TestCaseData(
						new PositionAnalysisParameters
						{
							MaxDepth = 18,
							MaxNodes = 3000000,
							SearchMoves = new[] { new CnMove("e2e4") }
						}
						).
						Returns("depth 18 nodes 3000000 searchmoves e2e4").
						SetName("MaxDepth + MaxNodes + SearchMoves");

					yield return new TestCaseData(
						new PositionAnalysisParameters { MovesToSearchMate = 3}
						).
						Returns("mate 3").
						SetName("MovesToSearchMate");

					yield return new TestCaseData(
						new PositionAnalysisParameters { Pondering = true}
						).
						Returns("ponder").
						SetName("Pondering");

					yield return new TestCaseData(
						new PositionAnalysisParameters { Pondering = true, MaxDepth = 10}
						).
						Returns("ponder depth 10").
						SetName("Pondering + MaxDepth");

					yield return new TestCaseData(
						new PositionAnalysisParameters { Pondering = true, TimeRestrictions = new InfiniteAnalysisTime()}
						).
						Returns("ponder infinite").
						SetName("Infinite");

					yield return new TestCaseData(
						new PositionAnalysisParameters { MaxDepth = 10, TimeRestrictions = new ExactAnalysisTime(1000)}
						).
						Returns("movetime 1000 depth 10").
						SetName("MoveTime");

					yield return new TestCaseData(
						new PositionAnalysisParameters { Pondering = true, TimeRestrictions = new ClockTime(240000, 300000)}
						).
						Returns("ponder wtime 240000 btime 300000").
						SetName("ClockTime");

					yield return new TestCaseData(
						new PositionAnalysisParameters
						{
							Pondering = true,
							TimeRestrictions = new ClockTime(240000, 300000, 40)
						}
						).
						Returns("ponder wtime 240000 btime 300000 movestogo 40").
						SetName("ClockTimeWithMovesToTimeControl");

					yield return new TestCaseData(
						new PositionAnalysisParameters
						{
							Pondering = true,
							TimeRestrictions = new ClockTime(240000, 300000, 3000, 4000)
						}
						).
						Returns("ponder wtime 240000 btime 300000 winc 3000 binc 4000").
						SetName("ClockTimeWithIncrements");

					yield return new TestCaseData(
						new PositionAnalysisParameters
						{
							Pondering = true,
							TimeRestrictions = new ClockTime(240000, 300000, 3000, 4000, 40)
						}
						).
						Returns("ponder wtime 240000 btime 300000 winc 3000 binc 4000 movestogo 40").
						SetName("ClockTimeWithIncrementsAndMovesToTimeControl");
				}
			}
		}
	}
}

﻿using System.Collections;

using CloudChess.Analysis.Uci;

using NUnit.Framework;

namespace CloudChess.UnitTesting
{
	public sealed class SetOptionUciCommandTests
	{
		/// <summary>
		/// Checks that CommandText property...
		/// </summary>
		[TestFixture]
		[Category("ChessAnalyzer/Uci/SetOptionUciCommandTests")]
		public sealed class TheCommandTextProperty
		{
			/// <summary>
			/// Gets correct command text.
			/// </summary>
			/// <param name="command">A "setoption" UCI-command.</param>
			/// <returns>Command text.</returns>
			[Test]
			[TestCaseSource("SetOptionTestSuite")]
			public string GetsCorrectCommandText(IUciCommand command)
			{
				return command.CommandText;
			}

			public static IEnumerable SetOptionTestSuite
			{
				get
				{
					yield return new TestCaseData(new SetOptionUciCommand("Option 1", "Value 1")).
						Returns("setoption name Option 1 value Value 1").
						SetName("setoption name Option 1 value Value 1");
					yield return new TestCaseData(new SetOptionUciCommand("Option 2", "")).
						Returns("setoption name Option 2 value <empty>").
						SetName("setoption name Option 2 value <empty>");
					yield return new TestCaseData(new SetOptionUciCommand("Option 3", null)).
						Returns("setoption name Option 3").
						SetName("setoption name Option 3");
				}
			}
		}
	}
}

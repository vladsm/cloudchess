﻿using System;
using System.Reactive;

using CloudChess.Analysis.Uci;

using Microsoft.Reactive.Testing;

using Moq;

namespace CloudChess.UnitTesting
{
	internal sealed class UciEngineMock :
		Mock<IUciEngine>,
		IDisposable
	{
		private readonly TestObservableRecorder<UciResponse> _responsesObservable;
		private readonly TestObservableRecorder<Unit> _closedObservable;
		private readonly TestScheduler _scheduler;

		public UciEngineMock() : this(new TestScheduler())
		{
		}

		public UciEngineMock(TestScheduler scheduler)
		{
			_scheduler = scheduler;
			_responsesObservable = new TestObservableRecorder<UciResponse>(_scheduler);
			_closedObservable = new TestObservableRecorder<Unit>(_scheduler)
			{
				ReactiveTestFixture.OnCompleted<Unit>(TimeSpan.FromDays(1))
			};

			Setup(engine => engine.Responses).Returns(_responsesObservable.Result);
			Setup(engine => engine.Closed).Returns(_closedObservable.Result);
		}

		public IObservableRecorder<UciResponse> ResponsesMessages
		{
			get { return _responsesObservable; }
		}

		public IObservableRecorder<Unit> ClosedMessages
		{
			get { return _closedObservable; }
		}

		public TestScheduler Scheduler
		{
			get { return _scheduler; }
		}

		public void Dispose()
		{
			_responsesObservable.Dispose();
			_closedObservable.Dispose();
		}
	}
}

﻿using System.Collections.Generic;
using System.Reactive;

using Microsoft.Reactive.Testing;

namespace CloudChess.UnitTesting
{
	internal interface IObservableRecorder<T>
	{
		void Add(Recorded<Notification<T>> recordedNotification);
		void Add(IEnumerable<Recorded<Notification<T>>> recordedNotifications);
	}


	internal static class ObservableRecorderExtensions
	{
		public static void Add<T>(
			this IObservableRecorder<T> recorder,
			params Recorded<Notification<T>>[] recordedNotifications
			)
		{
			recorder.Add(recordedNotifications);
		}
	}
}

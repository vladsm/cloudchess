﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reactive;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reactive.Subjects;

using Microsoft.Reactive.Testing;

namespace CloudChess.UnitTesting
{
// ReSharper disable PossibleInfiniteInheritance
	public sealed class TestObservableRecorder<T> :
		IEnumerable<Recorded<Notification<T>>>,
		IObservableRecorder<T>,
		IDisposable
// ReSharper restore PossibleInfiniteInheritance
	{
		private readonly TestScheduler _scheduler;
		private readonly Subject<T> _subject;
		private readonly List<Recorded<Notification<T>>> _innerList;

		public TestObservableRecorder(TestScheduler scheduler)
		{
			if (scheduler == null) throw new ArgumentNullException("scheduler");

			_scheduler = scheduler;
			_subject = new Subject<T>();
			_innerList = new List<Recorded<Notification<T>>>();
		}

		public IObservable<T> Result
		{
			get { return _subject.AsObservable(); }
		}

		public void Add(Recorded<Notification<T>> recordedNotification)
		{
			if (recordedNotification == null) throw new ArgumentNullException("recordedNotification");

			_innerList.Add(recordedNotification);
			_scheduler.Schedule(
				TimeSpan.FromTicks(recordedNotification.Time),
				() => recordedNotification.Value.Accept(_subject)
				);
		}

		public void Add(IEnumerable<Recorded<Notification<T>>> recordedNotifications)
		{
			if (recordedNotifications == null) throw new ArgumentNullException("recordedNotifications");
			foreach (var recordedNotification in recordedNotifications)
			{
				Add(recordedNotification);
			}
		}

		public IEnumerator<Recorded<Notification<T>>> GetEnumerator()
		{
			return _innerList.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public void Dispose()
		{
			_subject.Dispose();
		}
	}
}

﻿using System;
using System.Reactive;

using Microsoft.Reactive.Testing;

namespace CloudChess.UnitTesting
{
	public abstract class ReactiveTestFixture
	{
		internal static Recorded<Notification<T>> OnNext<T>(long ticks, T value)
		{
			return new Recorded<Notification<T>>(ticks, Notification.CreateOnNext(value));
		}

		internal static Recorded<Notification<T>> OnCompleted<T>(long ticks)
		{
			return new Recorded<Notification<T>>(ticks, Notification.CreateOnCompleted<T>());
		}

		internal static Recorded<Notification<T>> OnError<T>(long ticks, Exception error)
		{
			return new Recorded<Notification<T>>(ticks, Notification.CreateOnError<T>(error));
		}

		internal static Recorded<Notification<T>> OnError<T, TException>(long ticks)
			where TException : Exception, new()
		{
			return new Recorded<Notification<T>>(ticks, Notification.CreateOnError<T>(new TException()));
		}

		internal static Recorded<Notification<T>> OnNext<T>(TimeSpan timeSpan, T value)
		{
			return new Recorded<Notification<T>>(timeSpan.Ticks, Notification.CreateOnNext(value));
		}

		internal static Recorded<Notification<T>> OnCompleted<T>(TimeSpan timeSpan)
		{
			return new Recorded<Notification<T>>(timeSpan.Ticks, Notification.CreateOnCompleted<T>());
		}

		internal static Recorded<Notification<T>> OnError<T>(TimeSpan timeSpan, Exception error)
		{
			return new Recorded<Notification<T>>(timeSpan.Ticks, Notification.CreateOnError<T>(error));
		}

		internal static Recorded<Notification<T>> OnError<T, TException>(TimeSpan timeSpan)
			where TException : Exception, new()
		{
			return new Recorded<Notification<T>>(timeSpan.Ticks, Notification.CreateOnError<T>(new TException()));
		}
	}
}

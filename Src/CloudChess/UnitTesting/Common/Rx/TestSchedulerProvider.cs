﻿using System.Reactive.Concurrency;

using Microsoft.Reactive.Testing;

namespace CloudChess.UnitTesting
{
	internal sealed class TestSchedulerProvider : ISchedulerProvider
	{
		private readonly IScheduler _defaultScheduler = new TestScheduler();

		public IScheduler Default
		{
			get { return _defaultScheduler; }
		}
	}
}

﻿using System;

using Moq.Language.Flow;

namespace CloudChess.UnitTesting
{
	internal static class MoqSetupExtensions
	{
		public static ICallbackResult CallbackInOrder<TService, TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TArg10, TArg11, TArg12, TArg13, TArg14, TArg15>(
			this ISetup<TService> setupClause,
			Action<int, TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TArg10, TArg11, TArg12, TArg13, TArg14, TArg15> callback
			)
			where TService : class
		{
			int orderCounter = 0;
			return setupClause.Callback<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TArg10, TArg11, TArg12, TArg13, TArg14, TArg15>(
				(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15) =>
					callback(orderCounter++, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15)
				);
		}

		public static ICallbackResult CallbackInOrder<TService, TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TArg10, TArg11, TArg12, TArg13, TArg14>(
			this ISetup<TService> setupClause,
			Action<int, TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TArg10, TArg11, TArg12, TArg13, TArg14> callback
			)
			where TService : class
		{
			int orderCounter = 0;
			return setupClause.Callback<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TArg10, TArg11, TArg12, TArg13, TArg14>(
				(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14) =>
					callback(orderCounter++, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14)
				);
		}

		public static ICallbackResult CallbackInOrder<TService, TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TArg10, TArg11, TArg12, TArg13>(
			this ISetup<TService> setupClause,
			Action<int, TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TArg10, TArg11, TArg12, TArg13> callback
			)
			where TService : class
		{
			int orderCounter = 0;
			return setupClause.Callback<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TArg10, TArg11, TArg12, TArg13>(
				(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13) =>
					callback(orderCounter++, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13)
				);
		}

		public static ICallbackResult CallbackInOrder<TService, TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TArg10, TArg11, TArg12>(
			this ISetup<TService> setupClause,
			Action<int, TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TArg10, TArg11, TArg12> callback
			)
			where TService : class
		{
			int orderCounter = 0;
			return setupClause.Callback<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TArg10, TArg11, TArg12>(
				(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12) =>
					callback(orderCounter++, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12)
				);
		}

		public static ICallbackResult CallbackInOrder<TService, TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TArg10, TArg11>(
			this ISetup<TService> setupClause,
			Action<int, TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TArg10, TArg11> callback
			)
			where TService : class
		{
			int orderCounter = 0;
			return setupClause.Callback<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TArg10, TArg11>(
				(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11) =>
					callback(orderCounter++, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11)
				);
		}

		public static ICallbackResult CallbackInOrder<TService, TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TArg10>(
			this ISetup<TService> setupClause,
			Action<int, TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TArg10> callback
			)
			where TService : class
		{
			int orderCounter = 0;
			return setupClause.Callback<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9, TArg10>(
				(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10) =>
					callback(orderCounter++, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10)
				);
		}

		public static ICallbackResult CallbackInOrder<TService, TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9>(
			this ISetup<TService> setupClause,
			Action<int, TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9> callback
			)
			where TService : class
		{
			int orderCounter = 0;
			return setupClause.Callback<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8, TArg9>(
				(a1, a2, a3, a4, a5, a6, a7, a8, a9) =>
					callback(orderCounter++, a1, a2, a3, a4, a5, a6, a7, a8, a9)
				);
		}

		public static ICallbackResult CallbackInOrder<TService, TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8>(
			this ISetup<TService> setupClause,
			Action<int, TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8> callback
			)
			where TService : class
		{
			int orderCounter = 0;
			return setupClause.Callback<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7, TArg8>(
				(a1, a2, a3, a4, a5, a6, a7, a8) =>
					callback(orderCounter++, a1, a2, a3, a4, a5, a6, a7, a8)
				);
		}

		public static ICallbackResult CallbackInOrder<TService, TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7>(
			this ISetup<TService> setupClause,
			Action<int, TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7> callback
			)
			where TService : class
		{
			int orderCounter = 0;
			return setupClause.Callback<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6, TArg7>(
				(a1, a2, a3, a4, a5, a6, a7) =>
					callback(orderCounter++, a1, a2, a3, a4, a5, a6, a7)
				);
		}

		public static ICallbackResult CallbackInOrder<TService, TArg1, TArg2, TArg3, TArg4, TArg5, TArg6>(
			this ISetup<TService> setupClause,
			Action<int, TArg1, TArg2, TArg3, TArg4, TArg5, TArg6> callback
			)
			where TService : class
		{
			int orderCounter = 0;
			return setupClause.Callback<TArg1, TArg2, TArg3, TArg4, TArg5, TArg6>(
				(a1, a2, a3, a4, a5, a6) => callback(orderCounter++, a1, a2, a3, a4, a5, a6)
				);
		}

		public static ICallbackResult CallbackInOrder<TService, TArg1, TArg2, TArg3, TArg4, TArg5>(
			this ISetup<TService> setupClause,
			Action<int, TArg1, TArg2, TArg3, TArg4, TArg5> callback
			)
			where TService : class
		{
			int orderCounter = 0;
			return setupClause.Callback<TArg1, TArg2, TArg3, TArg4, TArg5>(
				(a1, a2, a3, a4, a5) => callback(orderCounter++, a1, a2, a3, a4, a5)
				);
		}

		public static ICallbackResult CallbackInOrder<TService, TArg1, TArg2, TArg3, TArg4>(
			this ISetup<TService> setupClause,
			Action<int, TArg1, TArg2, TArg3, TArg4> callback
			)
			where TService : class
		{
			int orderCounter = 0;
			return setupClause.Callback<TArg1, TArg2, TArg3, TArg4>(
				(a1, a2, a3, a4) => callback(orderCounter++, a1, a2, a3, a4)
				);
		}

		public static ICallbackResult CallbackInOrder<TService, TArg1, TArg2, TArg3>(
			this ISetup<TService> setupClause,
			Action<int, TArg1, TArg2, TArg3> callback
			)
			where TService : class
		{
			int orderCounter = 0;
			return setupClause.Callback<TArg1, TArg2, TArg3>((a1, a2, a3) => callback(orderCounter++, a1, a2, a3));
		}

		public static ICallbackResult CallbackInOrder<TService, TArg1, TArg2>(
			this ISetup<TService> setupClause,
			Action<int, TArg1, TArg2> callback
			)
			where TService : class
		{
			int orderCounter = 0;
			return setupClause.Callback<TArg1, TArg2>((a1, a2) => callback(orderCounter++, a1, a2));
		}

		public static ICallbackResult CallbackInOrder<TService, TArg1>(
			this ISetup<TService> setupClause,
			Action<int, TArg1> callback
			)
			where TService : class
		{
			int orderCounter = 0;
			return setupClause.Callback<TArg1>(a1 => callback(orderCounter++, a1));
		}

		public static ICallbackResult CallbackInOrder<TService>(
			this ISetup<TService> setupClause,
			Action<int> callback
			)
			where TService : class
		{
			int orderCounter = 0;
			return setupClause.Callback(() => callback(orderCounter++));
		}
	}
}

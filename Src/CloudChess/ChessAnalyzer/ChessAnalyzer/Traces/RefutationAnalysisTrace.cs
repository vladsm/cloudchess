using System;
using System.Collections.Generic;
using System.Linq;

using CloudChess.Analysis.Uci;

namespace CloudChess.Analysis
{
	/// <summary>
	/// Represents a refutation trace.
	/// </summary>
	public sealed class RefutationAnalysisTrace : AnalysisTrace, IEquatable<RefutationAnalysisTrace>
	{
		/// <summary>
		/// Gets a refutation.
		/// </summary>
		public IEnumerable<CnMove> Refutation { get; private set; }

		public RefutationAnalysisTrace(IEnumerable<CnMove> refutation)
		{
			if (refutation == null) throw new ArgumentNullException("refutation");
			var refutationAsArray = refutation as CnMove[] ?? refutation.ToArray();
			if (!refutationAsArray.Any()) throw new ArgumentException("Refutation is empty.", "refutation");

			Refutation = refutationAsArray;
		}

		internal RefutationAnalysisTrace(InfoUciResponse info)
		{
			if (info == null) throw new ArgumentNullException("info");
			if (info.Refutation == null || !info.Refutation.Any()) throw new ArgumentException("Refutation is not specified or empty.", "info");

			Refutation = info.Refutation;
		}

		#region Equality overridings

		public bool Equals(RefutationAnalysisTrace other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return Refutation.SequenceEqual(other.Refutation);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			var typedObj = obj as RefutationAnalysisTrace;
			return typedObj != null && Equals(typedObj);
		}

		public override int GetHashCode()
		{
			return Refutation.GetHashCode();
		}

		#endregion
	}
}

﻿using System;

using CloudChess.Analysis.Uci;

namespace CloudChess.Analysis
{
	public sealed class MessageAnalysisTrace : AnalysisTrace, IEquatable<MessageAnalysisTrace>
	{
		public string Message { get; private set; }

		public MessageAnalysisTrace(string message)
		{
			if (message == null) throw new ArgumentNullException("message");
			if (message.Length == 0) throw new ArgumentException("Message is empty.", "message");

			Message = message;
		}

		internal MessageAnalysisTrace(InfoUciResponse info)
		{
			if (info == null) throw new ArgumentNullException("info");
			if (string.IsNullOrEmpty(info.Message)) throw new ArgumentException("Message is not specified.", "info");

			Message = info.Message;
		}

		#region Equality overridings

		public bool Equals(MessageAnalysisTrace other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return string.Equals(Message, other.Message);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			var typedObj = obj as MessageAnalysisTrace;
			return typedObj != null && Equals(typedObj);
		}

		public override int GetHashCode()
		{
			return Message.GetHashCode();
		}

		#endregion
	}
}

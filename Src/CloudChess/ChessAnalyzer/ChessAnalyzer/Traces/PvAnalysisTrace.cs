using System;
using System.Collections.Generic;
using System.Linq;

using CloudChess.Analysis.Uci;

namespace CloudChess.Analysis
{
	/// <summary>
	/// Represents a principal variation trace.
	/// </summary>
	public sealed class PvAnalysisTrace : AnalysisTrace, IEquatable<PvAnalysisTrace>
	{
		/// <summary>
		/// Gets a principal variation produced by UCI-engine.
		/// </summary>
		public IEnumerable<CnMove> Pv { get; private set; }

		/// <summary>
		/// Gets a principal variation index for the multipv mode.
		/// </summary>
		public int PvIndex { get; private set; }

		/// <summary>
		/// Gets a position estimation;
		/// </summary>
		public PositionEstimation PositionEstimation { get; private set; }
		
		/// <summary>
		/// Gets the time searched in milliseconds.
		/// </summary>
		public int? Time { get; private set; }

		/// <summary>
		/// Gets the nodes searched.
		/// </summary>
		public int? Nodes { get; private set; }

		/// <summary>
		/// Gets a number of nodes searched per second.
		/// </summary>
		public int? NodesPerSecond { get; private set; }

		/// <summary>
		/// Gets a search depth in plies.
		/// </summary>
		public int? Depth { get; private set; }

		/// <summary>
		/// Gets a selective search depth in plies.
		/// </summary>
		public int? SelectiveDepth { get; private set; }

		/// <summary>
		/// Gets a hash fullness value in permill.
		/// </summary>
		public int? HashFull { get; private set; }

		/// <summary>
		/// Gets a number of positions found inthe endgame table.
		/// </summary>
		public int? EndgameTableHits { get; private set; }


		public PvAnalysisTrace(
			IEnumerable<CnMove> pv,
			int pvIndex,
			PositionEstimation positionEstimation,
			int? time,
			int? nodes,
			int? nodesPerSecond,
			int? depth,
			int? selectiveDepth,
			int? hashFull,
			int? tbHits
			)
		{
			if (pv == null) throw new ArgumentNullException("pv");

			var pvAsArray = pv.ToArray();
			if (!pvAsArray.Any()) throw new ArgumentException("Principal variation is empty", "pv");
			if (positionEstimation == null) throw new ArgumentNullException("positionEstimation");

			Pv = pvAsArray;
			PvIndex = pvIndex;
			PositionEstimation = positionEstimation;
			Time = time;
			Nodes = nodes;
			NodesPerSecond = nodesPerSecond;
			Depth = depth;
			SelectiveDepth = selectiveDepth;
			HashFull = hashFull;
			EndgameTableHits = tbHits;
		}

		internal PvAnalysisTrace(InfoUciResponse info)
		{
			if (info == null) throw new ArgumentNullException("info");
			if (info.Pv == null || !info.Pv.Any()) throw new ArgumentException("Principal variation is not specified or empty.", "info");
			if (info.PositionEstimation == null) throw new ArgumentException("Position estimation is not specified.", "info");

			Pv = info.Pv;
			PvIndex = info.MultiPv ?? 1;
			PositionEstimation = info.PositionEstimation;
			Time = info.Time;
			Nodes = info.Nodes;
			NodesPerSecond = info.NodesPerSecond;
			Depth = info.Depth;
			SelectiveDepth = info.SelectiveDepth;
			HashFull = info.HashFull;
			EndgameTableHits = info.TbHits;
		}

		#region Equality overridings

		public bool Equals(PvAnalysisTrace other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return Pv.SequenceEqual(other.Pv) &&
				PvIndex == other.PvIndex &&
				Equals(PositionEstimation, other.PositionEstimation) &&
				Time == other.Time &&
				Nodes == other.Nodes &&
				NodesPerSecond == other.NodesPerSecond &&
				Depth == other.Depth &&
				SelectiveDepth == other.SelectiveDepth &&
				EndgameTableHits == other.EndgameTableHits &&
				HashFull == other.HashFull;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			var typedObj = obj as PvAnalysisTrace;
			return typedObj != null && Equals(typedObj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				const int shift = 397;
				int hashCode = Pv.GetHashCode();
				hashCode = (hashCode*shift) ^ PvIndex;
				hashCode = (hashCode*shift) ^ PositionEstimation.GetHashCode();
				hashCode = (hashCode*shift) ^ Time.GetHashCode();
				hashCode = (hashCode*shift) ^ Nodes.GetHashCode();
				hashCode = (hashCode*shift) ^ NodesPerSecond.GetHashCode();
				hashCode = (hashCode*shift) ^ Depth.GetHashCode();
				hashCode = (hashCode*shift) ^ SelectiveDepth.GetHashCode();
				hashCode = (hashCode*shift) ^ EndgameTableHits.GetHashCode();
				hashCode = (hashCode*shift) ^ HashFull.GetHashCode();
				return hashCode;
			}
		}

		#endregion

	}
}

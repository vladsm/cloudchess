using System;
using System.Linq;

using CloudChess.Analysis.Uci;

namespace CloudChess.Analysis
{
	/// <summary>
	/// Represents currently calculating line.
	/// </summary>
	public sealed class CurrentLineAnalysisTrace : AnalysisTrace
	{
		/// <summary>
		/// Gets or set currently calculating line.
		/// </summary>
		public CurrentCalculatingLine CurrentLine { get; private set; }

		public CurrentLineAnalysisTrace(CurrentCalculatingLine currentLine)
		{
			if (currentLine == null) throw new ArgumentNullException("currentLine");
			CurrentLine = currentLine;
		}

		internal CurrentLineAnalysisTrace(InfoUciResponse info)
		{
			if (info == null) throw new ArgumentNullException("info");
			if (info.CurrentLine == null) throw new ArgumentException("Current line is not specified.", "info");

			CurrentLine = info.CurrentLine;
		}

		#region Equality overridings

		public bool Equals(CurrentLineAnalysisTrace other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return CurrentLine == null && other.CurrentLine == null ||
				CurrentLine != null && other.CurrentLine != null &&
					CurrentLine.CpuIndex == other.CurrentLine.CpuIndex &&
					CurrentLine.Moves.SequenceEqual(other.CurrentLine.Moves);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			var typedObj = obj as CurrentLineAnalysisTrace;
			return typedObj != null && Equals(typedObj);
		}

		public override int GetHashCode()
		{
			unchecked { return (CurrentLine.Moves.GetHashCode() * 397) ^ CurrentLine.CpuIndex.GetHashCode(); }
		}

		#endregion
	}
}

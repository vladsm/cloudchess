using System;

using CloudChess.Analysis.Uci;

namespace CloudChess.Analysis
{
	/// <summary>
	/// Represents trace describing the best move for the analysis.
	/// </summary>
	public sealed class BestMoveAnalysisTrace : AnalysisTrace, IEquatable<BestMoveAnalysisTrace>
	{
		/// <summary>
		/// Gets a best move.
		/// </summary>
		public CnMove BestMove { get; private set; }

		/// <summary>
		/// Gets a move to ponder on.
		/// </summary>
		public CnMove Ponder { get; private set; }


		public BestMoveAnalysisTrace(CnMove bestMove, CnMove ponder)
		{
			if (bestMove == null) throw new ArgumentNullException("bestMove");

			BestMove = bestMove;
			Ponder = ponder;
		}

		public BestMoveAnalysisTrace(CnMove bestMove) : this(bestMove, null)
		{
		}

		internal BestMoveAnalysisTrace(BestMoveUciResponse uciResponse)
		{
			if (uciResponse == null) throw new ArgumentNullException("uciResponse");
			BestMove = uciResponse.BestMove;
			Ponder = uciResponse.Ponder;
		}

		#region Equality overridings

		public bool Equals(BestMoveAnalysisTrace other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return Equals(BestMove, other.BestMove) && Equals(Ponder, other.Ponder);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			var typedObj = obj as BestMoveAnalysisTrace;
			return typedObj != null && Equals(typedObj);
		}

		public override int GetHashCode()
		{
			unchecked { return (BestMove.GetHashCode()*397) ^ (Ponder != null ? Ponder.GetHashCode() : 0); }
		}

		#endregion
	}
}

using System;

using CloudChess.Analysis.Uci;

namespace CloudChess.Analysis
{
	/// <summary>
	/// Represents an analysis progress.
	/// </summary>
	public sealed class ProgressAnalysisTrace : AnalysisTrace, IEquatable<ProgressAnalysisTrace>
	{
		/// <summary>
		/// Gets the time searched in milliseconds.
		/// </summary>
		public int Time { get; private set; }

		/// <summary>
		/// Gets the nodes searched.
		/// </summary>
		public int Nodes { get; private set; }

		/// <summary>
		/// Gets a number of nodes searched per second.
		/// </summary>
		public int NodesPerSecond { get; private set; }

		/// <summary>
		/// Gets a search depth in plies.
		/// </summary>
		public int? Depth { get; private set; }

		/// <summary>
		/// Gets a hash fullness value in permill.
		/// </summary>
		public int? HashFull { get; private set; }

		/// <summary>
		/// Gets a number of positions found in the endgame table.
		/// </summary>
		public int? EndgameTableHits { get; private set; }

		/// <summary>
		/// Gets a CPU usage in permill.
		/// </summary>
		public int? CpuLoad { get; set; }


		public ProgressAnalysisTrace(
			int time,
			int nodes,
			int nodesPerSecond,
			int? depth,
			int? hashFull,
			int? tbHits,
			int? cpuLoad
			)
		{
			Time = time;
			Nodes = nodes;
			NodesPerSecond = nodesPerSecond;
			Depth = depth;
			HashFull = hashFull;
			EndgameTableHits = tbHits;
			CpuLoad = cpuLoad;
		}

		internal ProgressAnalysisTrace(InfoUciResponse info)
		{
			if (info == null) throw new ArgumentNullException("info");
			if (!info.Time.HasValue) throw new ArgumentException("Time is not specified", "info");
			if (!info.Nodes.HasValue) throw new ArgumentException("Number of searched nodes is not specified", "info");
			
			Time = info.Time.Value;
			Nodes = info.Nodes.Value;
			NodesPerSecond = info.NodesPerSecond.HasValue ?
				info.NodesPerSecond.Value :
				info.Nodes.Value / (info.Time.Value/1000);
			Depth = info.Depth;
			HashFull = info.HashFull;
			EndgameTableHits = info.TbHits;
			CpuLoad = info.CpuLoad;
		}

		#region Equality overridings

		public bool Equals(ProgressAnalysisTrace other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return Time == other.Time && Nodes == other.Nodes && NodesPerSecond == other.NodesPerSecond && 
				Depth == other.Depth && HashFull == other.HashFull && EndgameTableHits == other.EndgameTableHits && 
				CpuLoad == other.CpuLoad;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			var typedObj = obj as ProgressAnalysisTrace;
			return typedObj != null && Equals(typedObj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				const int shift = 397;
				int hashCode = Time;
				hashCode = (hashCode*shift) ^ Nodes;
				hashCode = (hashCode*shift) ^ NodesPerSecond;
				hashCode = (hashCode*shift) ^ Depth.GetHashCode();
				hashCode = (hashCode*shift) ^ HashFull.GetHashCode();
				hashCode = (hashCode*shift) ^ EndgameTableHits.GetHashCode();
				hashCode = (hashCode*shift) ^ CpuLoad.GetHashCode();
				return hashCode;
			}
		}

		#endregion

	}
}

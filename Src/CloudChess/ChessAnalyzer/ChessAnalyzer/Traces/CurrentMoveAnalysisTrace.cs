using System;

using CloudChess.Analysis.Uci;

namespace CloudChess.Analysis
{
	/// <summary>
	/// Represents currently calculating move.
	/// </summary>
	public sealed class CurrentMoveAnalysisTrace : AnalysisTrace, IEquatable<CurrentMoveAnalysisTrace>
	{
		/// <summary>
		/// Gets a currently searching move.
		/// </summary>
		public CnMove CurrentMove { get; private set; }

		/// <summary>
		/// Gets currently searching move number (1-based).
		/// </summary>
		public int CurrentMoveNumber { get; private set; }

		public CurrentMoveAnalysisTrace(CnMove currentMove, int currentMoveNumber)
		{
			if (currentMove == null) throw new ArgumentNullException("currentMove");
			CurrentMove = currentMove;
			CurrentMoveNumber = currentMoveNumber;
		}

		internal CurrentMoveAnalysisTrace(InfoUciResponse info)
		{
			if (info == null) throw new ArgumentNullException("info");
			if (info.CurrentMove == null) throw new ArgumentException("Current move is not specified", "info");
			if (!info.CurrentMoveNumber.HasValue) throw new ArgumentException("Current move number is not specified", "info");

			CurrentMove = info.CurrentMove;
			CurrentMoveNumber = info.CurrentMoveNumber.Value;
		}

		#region Equality overridings

		public bool Equals(CurrentMoveAnalysisTrace other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return Equals(CurrentMove, other.CurrentMove) && CurrentMoveNumber == other.CurrentMoveNumber;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			var typedObj = obj as CurrentMoveAnalysisTrace;
			return typedObj != null && Equals(typedObj);
		}

		public override int GetHashCode()
		{
			unchecked { return ((CurrentMove != null ? CurrentMove.GetHashCode() : 0)*397) ^ CurrentMoveNumber; }
		}

		#endregion
	}
}

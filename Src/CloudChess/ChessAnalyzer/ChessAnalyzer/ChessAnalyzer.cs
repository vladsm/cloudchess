﻿using System;
using System.Collections.Generic;
using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using System.Threading.Tasks;

using CloudChess.Analysis.Uci;

namespace CloudChess.Analysis
{
	public class ChessAnalyzer : IDisposable
	{
		protected IUciEngine UciEngine { get; set; }
		protected ISchedulerProvider SchedulerProvider { get; set; }

		/// <summary>
		/// Gets the timeout duration to wait UCI engine while it responds on "uci" command with "uciok".
		/// </summary>
		public TimeSpan UciInitializationTimeout { get; set; }

		/// <summary>
		/// Gets the maximum number of retries to initialize UCI engine.
		/// </summary>
		public int UciInitializationMaximumRetries { get; set; }


		public ChessAnalyzer(string enginePath) : this()
		{
			if (enginePath == null) throw new ArgumentNullException("enginePath");

			UciEngine = new UciEngine(enginePath);
			SchedulerProvider = new SchedulerProvider();
		}

		protected ChessAnalyzer()
		{
			UciInitializationTimeout = TimeSpan.FromSeconds(30);
			UciInitializationMaximumRetries = 3;
		}

		internal ChessAnalyzer(IUciEngine uciEngine, ISchedulerProvider schedulerProvider) : this()
		{
			if (uciEngine == null) throw new ArgumentNullException("uciEngine");
			if (schedulerProvider == null) throw new ArgumentNullException("schedulerProvider");

			UciEngine = uciEngine;
			SchedulerProvider = schedulerProvider;
		}


		public Task Run()
		{
			G.WrapExceptions<ChessAnalyzerInitializationException>(
				() => UciEngine.Run(),
				"Cannot run chess analyzer."
				);
			UciEngine.Closed.Subscribe(_ => Dispose());

			return InitializeUci().Concat(Observable.Return<UciResponse>(null)).ToTask();
		}

		private IObservable<UciResponse> InitializeUci()
		{
			// TODO: Later this method will return options and ids.

			return Observable.
				Create<UciResponse>(observer =>
					{
						var result = UciEngine.Responses.
							// Take all "id"s and "option"s responses...
							Where(r => r is IdUciResponse || r is OptionUciResponse).
							// ...splitted by "uciok" responses into windows.
							Window(
								UciEngine.Responses.
									Where(r => r is UciOkResponse).
									// If window wasn't closed in time raise timeout exception.
									Timeout(UciInitializationTimeout, SchedulerProvider.Default)
							).
							// Take the first window only
							Take(1).
							// Flatten the window to have raw responses ("id"s and "option"s)
							Switch().
							Subscribe(observer);

						UciEngine.SendCommand(UciCommands.InitUci);
						return result;
					}
					).
				// Retry sending "uci"-command if there are errors (normally timeout while waiting "uciok" response)
				Retry(UciInitializationMaximumRetries).
				Catch<UciResponse, TimeoutException>(exception =>
					Observable.Throw<UciResponse>(
						new ChessAnalyzerInitializationException("UCI engine timed out while UCI-mode initialization.", exception)
						)
					).
				Catch<UciResponse, Exception>(exception =>
					Observable.Throw<UciResponse>(new ChessAnalyzerInitializationException(exception.Message, exception))
					);
		}

		public IObservable<Unit> IsReady()
		{
			return Observable.Create<Unit>(
				observer =>
					{
						var result = UciEngine.Responses.
							Where(r => r is ReadyOkUciResponse).
							Take(1).
							Select(_ => Unit.Default).
							Subscribe(observer);
						
						UciEngine.SendCommand(UciCommands.IsReady);
						return result;
					}
				);
		}

		public IObservable<Unit> NewGame()
		{
			return Observable.Create<Unit>(observer =>
				{
					// No side effect from UCI-engine is expected
					var result = Observable.Empty<Unit>().Subscribe(observer);
					UciEngine.SendCommand(UciCommands.NewGame);
					return result;
				}
				);
		}

		public void SetPosition(IUciPosition position)
		{
			UciEngine.SendCommand(UciCommands.Position(position));
		}

		public IObservable<AnalysisTrace> Analyze(PositionAnalysisParameters positionAnalysisParameters)
		{
			return Observable.Create<AnalysisTrace>(observer =>
				{
					var result = UciEngine.Responses.
						TakeWhile(uciResponse => !(uciResponse is BestMoveUciResponse)).
						OfType<InfoUciResponse>().
						SelectMany(ToAnalysisTraces).
						Merge(UciEngine.Responses.OfType<BestMoveUciResponse>().Take(1).Select(ToAnalysisTrace)).
						Subscribe(observer);
					
					UciEngine.SendCommand(UciCommands.Go(positionAnalysisParameters));
					return result;
				});
		}

		private static BestMoveAnalysisTrace ToAnalysisTrace(BestMoveUciResponse bestMoveUciResponse)
		{
			return new BestMoveAnalysisTrace(bestMoveUciResponse);
		}

		private static IEnumerable<AnalysisTrace> ToAnalysisTraces(InfoUciResponse info)
		{
			if (info.Pv != null && info.PositionEstimation != null)
			{
				yield return new PvAnalysisTrace(info);
				yield break;
			}
			if (info.CurrentLine != null)
			{
				yield return new CurrentLineAnalysisTrace(info);
			}
			if (info.CurrentMove != null && info.CurrentMoveNumber.HasValue)
			{
				yield return new CurrentMoveAnalysisTrace(info);
			}
			if (info.Time.HasValue && info.Nodes.HasValue)
			{
				yield return new ProgressAnalysisTrace(info);
			}
			if (info.Refutation != null)
			{
				yield return new RefutationAnalysisTrace(info);
			}
			if (!string.IsNullOrEmpty(info.Message))
			{
				yield return new MessageAnalysisTrace(info);
			}
		}


		public void StopAnalyzing()
		{
			UciEngine.SendCommand(UciCommands.Stop);
		}

		public void SetOption(string name)
		{
			SetOption(name, null);
		}

		public void SetOption(string name, string value)
		{
			if (name == null) throw new ArgumentNullException("name");

			UciEngine.SendCommand(UciCommands.SetOption(name, value));
		}


		// TODO: Close notification required

		public Task Close()
		{
			var task = UciEngine.Closed.FirstAsync().ToTask();
			UciEngine.SendCommand(UciCommands.Quit);
			return task;
		}

		#region IDisposable implementation

		~ChessAnalyzer()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!disposing) return;
			if (UciEngine == null) return;

			UciEngine.Dispose();
			UciEngine = null;
		}

		#endregion
	}


	public abstract class ChessAnalyzer<TUciEngine> : ChessAnalyzer
		where TUciEngine : IUciEngine
	{
		protected ChessAnalyzer(string enginePath, Func<string, TUciEngine> engineCreator)
		{
			if (enginePath == null) throw new ArgumentNullException("enginePath");
			if (engineCreator == null) throw new ArgumentNullException("engineCreator");

			SchedulerProvider = new SchedulerProvider();
			UciEngine = engineCreator(enginePath);
		}

		internal protected ChessAnalyzer(IUciEngine uciEngine, ISchedulerProvider schedulerProvider) :
			base(uciEngine, schedulerProvider)
		{
		}
	}
}

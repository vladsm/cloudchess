﻿using System;
using System.Runtime.Serialization;

namespace CloudChess.Analysis
{
	[Serializable]
	public class ChessAnalyzerInitializationException : Exception
	{
		public ChessAnalyzerInitializationException()
		{
		}

		public ChessAnalyzerInitializationException(string message)
			: base(message)
		{
		}

		public ChessAnalyzerInitializationException(string message, Exception inner)
			: base(message, inner)
		{
		}

		protected ChessAnalyzerInitializationException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}
}

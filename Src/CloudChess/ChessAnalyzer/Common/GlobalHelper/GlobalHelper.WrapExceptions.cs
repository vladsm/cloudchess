﻿using System;

namespace CloudChess
{
	public static class G
	{
		/// <summary>
		/// Executes an action and wraps the <see cref="TSourceException"/> exception occured into new exception.
		/// </summary>
		/// <typeparam name="TSourceException">The type of the exceptions to be catched and wraped.</typeparam>
		/// <typeparam name="TTargetException">The target exception type to be wrapped into.</typeparam>
		/// <param name="action">An action to be executed.</param>
		/// <param name="exceptionWrapper">An exception wrapper function.</param>
		public static void WrapExceptions<TSourceException, TTargetException>(
			Action action,
			Func<TSourceException, TTargetException> exceptionWrapper
			)
			where TSourceException : Exception
			where TTargetException : Exception, new()
		{
			if (action == null) throw new ArgumentNullException("action");
			if (exceptionWrapper == null) throw new ArgumentNullException("exceptionWrapper");

			try
			{
				action();
			}
			catch (TSourceException exception)
			{
				throw exceptionWrapper(exception);
			}
		}

		/// <summary>
		/// Executes an action and wraps the <see cref="TSourceException"/> exception occured into new exception.
		/// </summary>
		/// <typeparam name="TSourceException">The type of the exceptions to be catched and wraped.</typeparam>
		/// <typeparam name="TTargetException">
		/// The target exception type to be wrapped into. The type should be implmented according to
		/// Exception-type pattern.
		/// </typeparam>
		/// <param name="action">An action to be executed.</param>
		/// <param name="newExceptionMessage">A message for the new exception.</param>
		/// <remarks>
		/// The implementation uses <see cref="Activator.CreateInstance(System.Type,object[])"/> to instantiate
		/// the new exception instance so some performance degradation should be considered. But for the case of
		/// exception it's supposed it will not affect in most cases.In case the high performance is required
		/// just don't use this overload or handle exceptions with try/catch.
		/// </remarks>
		public static void WrapExceptions<TSourceException, TTargetException>(
			Action action,
			string newExceptionMessage
			)
			where TSourceException : Exception
			where TTargetException : Exception, new()
		{
			WrapExceptions<TSourceException, TTargetException>(
				action,
				e =>
				{
					// Yes, it is taken into account the Reflection performance is weak.
					// But for the case of exception it's supposed it will not affect in most cases.
					// In case the high performance is required just don't use this overload or handle
					// exceptions with try/catch.
					var args = string.IsNullOrEmpty(newExceptionMessage) ?
						new object[] { e } :
						new object[] { newExceptionMessage, e };
					return (TTargetException)Activator.CreateInstance(typeof(TTargetException), args);
				}
				);
		}

		/// <summary>
		/// Executes an action and wraps the <see cref="TSourceException"/> exception occured into new exception.
		/// </summary>
		/// <typeparam name="TSourceException">The type of the exceptions to be catched and wraped.</typeparam>
		/// <typeparam name="TTargetException">
		/// The target exception type to be wrapped into.The type should be implmented according to
		/// Exception-type pattern.
		/// </typeparam>
		/// <param name="action">An action to be executed.</param>
		/// <remarks>
		/// The implementation uses <see cref="Activator.CreateInstance(System.Type,object[])"/> to instantiate
		/// the new exception instance so some performance degradation should be considered. But for the case of
		/// exception it's supposed it will not affect in most cases.In case the high performance is required
		/// just don't use this overload or handle exceptions with try/catch.
		/// </remarks>
		public static void WrapExceptions<TSourceException, TTargetException>(Action action)
			where TSourceException : Exception
			where TTargetException : Exception, new()
		{
			WrapExceptions<TSourceException, TTargetException>(action, (string)null);
		}

		/// <summary>
		/// Executes an action and wraps any exception occured into new exception.
		/// </summary>
		/// <typeparam name="TTargetException">The target exception type to be wrapped into.</typeparam>
		/// <param name="action">An action to be executed.</param>
		/// <param name="exceptionWrapper">An exception wrapper function.</param>
		public static void WrapExceptions<TTargetException>(
			Action action,
			Func<Exception, TTargetException> exceptionWrapper
			)
			where TTargetException : Exception, new()
		{
			WrapExceptions<Exception, TTargetException>(action, exceptionWrapper);
		}

		/// <summary>
		/// Executes an action and wraps any exception occured into new exception.
		/// </summary>
		/// <typeparam name="TTargetException">
		/// The target exception type to be wrapped into. The type should be implmented according to
		/// Exception-type pattern.
		/// </typeparam>
		/// <param name="action">An action to be executed.</param>
		/// <param name="newExceptionMessage">A message for the new exception.</param>
		/// <remarks>
		/// The implementation uses <see cref="Activator.CreateInstance(System.Type,object[])"/> to instantiate
		/// the new exception instance so some performance degradation should be considered. But for the case of
		/// exception it's supposed it will not affect in most cases.In case the high performance is required
		/// just don't use this overload or handle exceptions with try/catch.
		/// </remarks>
		public static void WrapExceptions<TTargetException>(
			Action action,
			string newExceptionMessage
			)
			where TTargetException : Exception, new()
		{
			WrapExceptions<Exception, TTargetException>(action, newExceptionMessage);
		}

		/// <summary>
		/// Executes an action and wraps any exception occured into new exception.
		/// </summary>
		/// <typeparam name="TTargetException">
		/// The target exception type to be wrapped into.The type should be implmented according to
		/// Exception-type pattern.
		/// </typeparam>
		/// <param name="action">An action to be executed.</param>
		/// <remarks>
		/// The implementation uses <see cref="Activator.CreateInstance(System.Type,object[])"/> to instantiate
		/// the new exception instance so some performance degradation should be considered. But for the case of
		/// exception it's supposed it will not affect in most cases.In case the high performance is required
		/// just don't use this overload or handle exceptions with try/catch.
		/// </remarks>
		public static void WrapExceptions<TTargetException>(Action action)
			where TTargetException : Exception, new()
		{
			WrapExceptions<Exception, TTargetException>(action);
		}

		/// <summary>
		/// Executes a function and wraps the <see cref="TSourceException"/> exception occured into new exception.
		/// </summary>
		/// <typeparam name="TResult">The function result type.</typeparam>
		/// <typeparam name="TSourceException">The type of the exceptions to be catched and wraped.</typeparam>
		/// <typeparam name="TTargetException">The target exception type to be wrapped into.</typeparam>
		/// <param name="func">An action to be executed.</param>
		/// <param name="exceptionWrapper">An exception wrapper function.</param>
		public static TResult WrapExceptions<TResult, TSourceException, TTargetException>(
			Func<TResult> func,
			Func<TSourceException, TTargetException> exceptionWrapper
			)
			where TSourceException : Exception
			where TTargetException : Exception, new()
		{
			if (func == null) throw new ArgumentNullException("func");
			if (exceptionWrapper == null) throw new ArgumentNullException("exceptionWrapper");

			try
			{
				return func();
			}
			catch (TSourceException exception)
			{
				throw exceptionWrapper(exception);
			}
		}

		/// <summary>
		/// Executes a function and wraps the <see cref="TSourceException"/> exception occured into new exception.
		/// </summary>
		/// <typeparam name="TResult">The function result type.</typeparam>
		/// <typeparam name="TSourceException">The type of the exceptions to be catched and wraped.</typeparam>
		/// <typeparam name="TTargetException">
		/// The target exception type to be wrapped into. The type should be implmented according to
		/// Exception-type pattern.
		/// </typeparam>
		/// <param name="func">An action to be executed.</param>
		/// <param name="newExceptionMessage">A message for the new exception.</param>
		/// <remarks>
		/// The implementation uses <see cref="Activator.CreateInstance(System.Type,object[])"/> to instantiate
		/// the new exception instance so some performance degradation should be considered. But for the case of
		/// exception it's supposed it will not affect in most cases.In case the high performance is required
		/// just don't use this overload or handle exceptions with try/catch.
		/// </remarks>
		public static TResult WrapExceptions<TResult, TSourceException, TTargetException>(
			Func<TResult> func,
			string newExceptionMessage
			)
			where TSourceException : Exception
			where TTargetException : Exception, new()
		{
			return WrapExceptions<TResult, TSourceException, TTargetException>(
				func,
				e =>
				{
					// Yes, it is taken into account the Reflection performance is weak.
					// But for the case of exception it's supposed it will not affect in most cases.
					// In case the high performance is required just don't use this overload or handle
					// exceptions with try/catch.
					var args = string.IsNullOrEmpty(newExceptionMessage) ?
						new object[] { e } :
						new object[] { newExceptionMessage, e };
					return (TTargetException)Activator.CreateInstance(typeof(TTargetException), args);
				}
				);
		}

		/// <summary>
		/// Executes a function and wraps the <see cref="TSourceException"/> exception occured into new exception.
		/// </summary>
		/// <typeparam name="TResult">The function result type.</typeparam>
		/// <typeparam name="TSourceException">The type of the exceptions to be catched and wraped.</typeparam>
		/// <typeparam name="TTargetException">
		/// The target exception type to be wrapped into.The type should be implmented according to
		/// Exception-type pattern.
		/// </typeparam>
		/// <param name="func">An action to be executed.</param>
		/// <remarks>
		/// The implementation uses <see cref="Activator.CreateInstance(System.Type,object[])"/> to instantiate
		/// the new exception instance so some performance degradation should be considered. But for the case of
		/// exception it's supposed it will not affect in most cases.In case the high performance is required
		/// just don't use this overload or handle exceptions with try/catch.
		/// </remarks>
		public static TResult WrapExceptions<TResult, TSourceException, TTargetException>(Func<TResult> func)
			where TSourceException : Exception
			where TTargetException : Exception, new()
		{
			return WrapExceptions<TResult, TSourceException, TTargetException>(func, (string)null);
		}

		/// <summary>
		/// Executes a function and wraps any exception occured into new exception.
		/// </summary>
		/// <typeparam name="TResult">The function result type.</typeparam>
		/// <typeparam name="TTargetException">The target exception type to be wrapped into.</typeparam>
		/// <param name="func">An action to be executed.</param>
		/// <param name="exceptionWrapper">An exception wrapper function.</param>
		public static TResult WrapExceptions<TResult, TTargetException>(
			Func<TResult> func,
			Func<Exception, TTargetException> exceptionWrapper
			)
			where TTargetException : Exception, new()
		{
			return WrapExceptions<TResult, Exception, TTargetException>(func, exceptionWrapper);
		}

		/// <summary>
		/// Executes a function and wraps any exception occured into new exception.
		/// </summary>
		/// <typeparam name="TResult">The function result type.</typeparam>
		/// <typeparam name="TTargetException">
		/// The target exception type to be wrapped into. The type should be implmented according to
		/// Exception-type pattern.
		/// </typeparam>
		/// <param name="func">An action to be executed.</param>
		/// <param name="newExceptionMessage">A message for the new exception.</param>
		/// <remarks>
		/// The implementation uses <see cref="Activator.CreateInstance(System.Type,object[])"/> to instantiate
		/// the new exception instance so some performance degradation should be considered. But for the case of
		/// exception it's supposed it will not affect in most cases.In case the high performance is required
		/// just don't use this overload or handle exceptions with try/catch.
		/// </remarks>
		public static TResult WrapExceptions<TResult, TTargetException>(
			Func<TResult> func,
			string newExceptionMessage
			)
			where TTargetException : Exception, new()
		{
			return WrapExceptions<TResult, Exception, TTargetException>(func, newExceptionMessage);
		}

		/// <summary>
		/// Executes a function and wraps any exception occured into new exception.
		/// </summary>
		/// <typeparam name="TResult">The function result type.</typeparam>
		/// <typeparam name="TTargetException">
		/// The target exception type to be wrapped into.The type should be implmented according to
		/// Exception-type pattern.
		/// </typeparam>
		/// <param name="func">An action to be executed.</param>
		/// <remarks>
		/// The implementation uses <see cref="Activator.CreateInstance(System.Type,object[])"/> to instantiate
		/// the new exception instance so some performance degradation should be considered. But for the case of
		/// exception it's supposed it will not affect in most cases.In case the high performance is required
		/// just don't use this overload or handle exceptions with try/catch.
		/// </remarks>
		public static TResult WrapExceptions<TResult, TTargetException>(Func<TResult> func)
			where TTargetException : Exception, new()
		{
			return WrapExceptions<TResult, Exception, TTargetException>(func);
		}
	}
}

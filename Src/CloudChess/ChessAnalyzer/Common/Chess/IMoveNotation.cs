﻿namespace CloudChess
{
	/// <summary>
	/// Describes the common interface for chess move notation.
	/// </summary>
	public interface IMoveNotation
	{
		/// <summary>
		/// Represents the move in text notation.
		/// </summary>
		string Notation { get; }
	}
}

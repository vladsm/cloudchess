﻿using System;

namespace CloudChess
{
	/// <summary>
	/// Represents the move in Coordinate Notation.
	/// </summary>
	/// <remarks>
	/// For example,
	/// <list type="bullet">
	///   <item><description>e2e4 - for pawn;</description></item>
	///   <item><description>g1f3 - for knight;</description></item>
	///   <item><description>d7d8q - for pawn promotion.</description></item>
	/// </list>
	/// </remarks>
	public sealed class CnMove : IMoveNotation, IEquatable<CnMove>
	{
		private readonly string _notation;

		public CnMove(string cnMoveNotation) : this(cnMoveNotation, false)
		{
		}

		public CnMove(string cnMoveNotation, bool validate)
		{
			if (cnMoveNotation == null) throw new ArgumentNullException("cnMoveNotation");
			if (validate && !Validate(cnMoveNotation))
			{
				throw new ArgumentException("Move notation is invalid Coordinate Notation.", "cnMoveNotation");
			}
			_notation = cnMoveNotation;
		}

		public string Notation
		{
			get { return _notation; }
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "cnMoveNotation")]
		public static bool Validate(string cnMoveNotation)
		{
			throw new NotImplementedException("Coordinate Notation move validation is not implemented yet.");
		}

		#region Equality overridings

		public bool Equals(CnMove other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return string.Equals(_notation, other._notation);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			var typedObj = obj as CnMove;
			return typedObj != null && Equals(typedObj);
		}

		public override int GetHashCode()
		{
			return _notation.GetHashCode();
		}

		public static bool operator ==(CnMove left, CnMove right)
		{
			return Equals(left, right);
		}

		public static bool operator !=(CnMove left, CnMove right)
		{
			return !Equals(left, right);
		}

		#endregion
	}
}

﻿using System;

namespace CloudChess
{
	/// <summary>
	/// Represents a base type for the chess positions estimations types.
	/// </summary>
	public abstract class PositionEstimation
	{
	}


	/// <summary>
	/// Represents the position estimation when the mate has been found.
	/// </summary>
	public sealed class MatePrediction : PositionEstimation, IEquatable<MatePrediction>
	{
		/// <summary>
		/// Gets a number of moves to mate. If the opponent is getting mated the value is negative.
		/// </summary>
		public int MovesToMate { get; private set; }

		public MatePrediction(int movesToMate)
		{
			MovesToMate = movesToMate;
		}

		#region Equality overridings

		public bool Equals(MatePrediction other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return MovesToMate == other.MovesToMate;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			var typedObj = obj as MatePrediction;
			return typedObj != null && Equals(typedObj);
		}

		public override int GetHashCode()
		{
			return MovesToMate;
		}

		public static bool operator ==(MatePrediction left, MatePrediction right)
		{
			return Equals(left, right);
		}

		public static bool operator !=(MatePrediction left, MatePrediction right)
		{
			return !Equals(left, right);
		}

		#endregion
	}


	/// <summary>
	/// Represents the position score.
	/// </summary>
	public sealed class PositionScore : PositionEstimation, IEquatable<PositionScore>
	{
		/// <summary>
		/// Gets a position score in centipawns. If the opponent has advance the value is negative.
		/// </summary>
		public int Cp { get; private set; }

		/// <summary>
		/// Gets a position score type.
		/// </summary>
		public PositionScoreType ScoreType { get; private set; }


		public PositionScore(int cp) : this(cp, PositionScoreType.Exact)
		{
		}

		public PositionScore(int cp, PositionScoreType scoreType)
		{
			Cp = cp;
			ScoreType = scoreType;
		}

		#region Equality overridings

		public bool Equals(PositionScore other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return Cp == other.Cp && ScoreType == other.ScoreType;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			var typedObj = obj as PositionScore;
			return typedObj != null && Equals(typedObj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return (Cp*397) ^ (int)ScoreType;
			}
		}

		public static bool operator ==(PositionScore left, PositionScore right)
		{
			return Equals(left, right);
		}

		public static bool operator !=(PositionScore left, PositionScore right)
		{
			return !Equals(left, right);
		}

		#endregion
	}


	/// <summary>
	/// Represents the type of the position score.
	/// </summary>
	public enum PositionScoreType
	{
		/// <summary>
		/// Position score is exact.
		/// </summary>
		Exact,

		/// <summary>
		/// Position score is lower bound.
		/// </summary>
		LowerBound,

		/// <summary>
		/// Position score is upper bound.
		/// </summary>
		UpperBound
	}
}

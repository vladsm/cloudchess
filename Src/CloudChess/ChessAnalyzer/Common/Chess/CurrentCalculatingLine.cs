﻿using System;
using System.Collections.Generic;

namespace CloudChess.Analysis
{
	/// <summary>
	/// Represents the line currently calculating by UCI-engine.
	/// </summary>
	public sealed class CurrentCalculatingLine
	{
		/// <summary>
		/// Gets or sets the CPU the line is calculating at.
		/// In case the single CPU is using the value is null.
		/// </summary>
		public int? CpuIndex { get; set; }

		/// <summary>
		/// Gets or sets moves in a line.
		/// </summary>
		public IEnumerable<CnMove> Moves { get; private set; }


		public CurrentCalculatingLine(IEnumerable<CnMove> moves)
		{
			if (moves == null) throw new ArgumentNullException("moves");
			Moves = moves;
		}

		public CurrentCalculatingLine(int cpuIndex, IEnumerable<CnMove> moves)
		{
			if (moves == null) throw new ArgumentNullException("moves");
			CpuIndex = cpuIndex;
			Moves = moves;
		}
	}
}

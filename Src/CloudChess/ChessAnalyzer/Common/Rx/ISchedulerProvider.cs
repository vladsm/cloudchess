﻿using System.Reactive.Concurrency;

namespace CloudChess
{
	public interface ISchedulerProvider
	{
		IScheduler Default { get; }
	}
}

﻿using System;

namespace CloudChess
{
	/// <summary>
	/// Provides methods for creating <see cref="IObservableCommand{T}"/> objects.
	/// </summary>
	public static class ObservableCommand
	{
		/// <summary>
		/// Creates the observable comand objects.
		/// </summary>
		/// <typeparam name="T">The type of the element of the observable result.</typeparam>
		/// <param name="executeAction">The action which is called on command execution.</param>
		/// <param name="result">The resulted observable sequence.</param>
		public static IObservableCommand<T> Create<T>(Action executeAction, IObservable<T> result)
		{
			return new SimpleObservableCommand<T>(executeAction, result);
		}

		#region SimpleObservableCommand class

		private class SimpleObservableCommand<T> : IObservableCommand<T>
		{
			private readonly Action _executeAction;
			private readonly IObservable<T> _result;

			public SimpleObservableCommand(Action executeAction, IObservable<T> result)
			{
				if (executeAction == null) throw new ArgumentNullException("executeAction");
				if (result == null) throw new ArgumentNullException("result");

				_executeAction = executeAction;
				_result = result;
			}

			public IDisposable Subscribe(IObserver<T> observer)
			{
				return _result.Subscribe(observer);
			}

			public void Execute()
			{
				_executeAction();
			}
		}

		#endregion
	}
}

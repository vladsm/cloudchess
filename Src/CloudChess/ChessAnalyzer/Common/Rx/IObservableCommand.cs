﻿using System;

namespace CloudChess
{
	/// <summary>
	/// Represents the command with the observable result.
	/// </summary>
	/// <typeparam name="T">The type of the element of the observable result.</typeparam>
	/// <remarks>
	/// The main use-case of this type is firstly to subscribe on the command result (the command object
	/// itself) and secondly execute the command.
	/// </remarks>
	public interface IObservableCommand<out T> : IObservable<T>
	{
		/// <summary>
		/// Executes the command.
		/// </summary>
		void Execute();
	}
}

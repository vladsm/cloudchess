﻿using System;
using System.Reactive.Concurrency;
using System.Reactive.Linq;

namespace CloudChess
{
	public static class ObservableExtensions
	{
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters")]
		public static IObservable<T> TraceToConsole<T>(
			this IObservable<T> source,
			string prefix,
			bool withTimestamp,
			IScheduler scheduler
			)
		{
			if (source == null) throw new ArgumentNullException("source");

			if (withTimestamp)
			{
				var result = scheduler == null ? source.Timestamp() : source.Timestamp(scheduler);
				return result.
					Do(
						timestamped => Console.WriteLine(@"[{0}] {1}{2}", timestamped.Timestamp, prefix, timestamped.Value),
						error => Console.WriteLine(@"{0}{1}", prefix, error),
						() => Console.WriteLine(@"{0}completed", prefix)
						).
					Select(item => item.Value);
			}

			return source.Do(
				item => Console.WriteLine(@"{0}{1}", prefix, item),
				error => Console.WriteLine(@"{0}{1}", prefix, error),
				() => Console.WriteLine(@"{0}completed", prefix)
				);
		}

		public static IObservable<T> TraceToConsole<T>(
			this IObservable<T> source,
			bool withTimestamp,
			IScheduler scheduler
			)
		{
			return TraceToConsole(source, "", withTimestamp, scheduler);
		}

		public static IObservable<T> TraceToConsole<T>(
			this IObservable<T> source,
			bool withTimestamp
			)
		{
			return TraceToConsole(source, "", withTimestamp, null);
		}

		public static IObservable<T> TraceToConsole<T>(
			this IObservable<T> source,
			string prefix,
			bool withTimestamp
			)
		{
			return TraceToConsole(source, prefix, withTimestamp, null);
		}

		public static IObservable<T> TraceToConsole<T>(this IObservable<T> source, string prefix)
		{
			return TraceToConsole(source, prefix, false);
		}

		public static IObservable<T> TraceToConsole<T>(this IObservable<T> source)
		{
			return TraceToConsole(source, "", false);
		}
	}
}

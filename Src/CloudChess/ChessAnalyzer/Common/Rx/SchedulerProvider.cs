﻿using System.Reactive.Concurrency;

namespace CloudChess
{
	public sealed class SchedulerProvider : ISchedulerProvider
	{
		public IScheduler Default
		{
			get { return DefaultScheduler.Instance; }
		}
	}
}

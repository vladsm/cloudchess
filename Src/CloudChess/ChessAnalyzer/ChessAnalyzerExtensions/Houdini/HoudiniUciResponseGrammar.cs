﻿using System;

using CloudChess.Analysis.Uci.Parsing;

using Irony.Parsing;

namespace CloudChess.Analysis.Houdini
{
	[Language("UCI response, Houdini dialect")]
	public class HoudiniUciResponseGrammar : UciResponseGrammar
	{
		public HoudiniUciResponseGrammar()
		{
			// Terminals
			var idle = ToTerm("idle");
			var idleValueTerminal = new NumberLiteral("UnsignedInteger", NumberOptions.IntOnly, typeof(IntegerNode))
			{
				DefaultIntTypes = new[] {TypeCode.UInt32}
			};
			idleValueTerminal.AddSuffix("M", TypeCode.UInt32);

			// NonTerminals
			var infoIdleItem = new NonTerminal("InfoIdleItem", typeof(InfoSkipNode));

			// BNF rules
			InfoItemRule |= infoIdleItem;
			infoIdleItem.Rule = idle + idleValueTerminal;

			// Other grammar setups
			MarkPunctuation(idle);
		}
	}
}

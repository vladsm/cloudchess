﻿using CloudChess.Analysis.Uci;

namespace CloudChess.Analysis.Houdini
{
	public class HoudiniUciEngine : UciEngine
	{
		public HoudiniUciEngine(string path) :
			base(path, new UciResponseParser<HoudiniUciResponseGrammar>())
		{
		}
	}
}

﻿using CloudChess.Analysis.Uci;

namespace CloudChess.Analysis.Houdini
{
	public sealed class HoudiniChessAnalyzer : ChessAnalyzer<HoudiniUciEngine>
	{
		internal HoudiniChessAnalyzer(IUciEngine uciEngine, ISchedulerProvider schedulerProvider) :
			base(uciEngine, schedulerProvider)
		{
		}

		public HoudiniChessAnalyzer(string enginePath) :
			base(enginePath, path => new HoudiniUciEngine(path))
		{
		}
	}
}

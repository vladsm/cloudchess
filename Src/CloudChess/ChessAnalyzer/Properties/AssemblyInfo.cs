﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("CloudChess.ChessAnalyzer")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

[assembly: CLSCompliant(true)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("19d80aa2-fa54-42a1-b5ef-7399ac1285c3")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

[assembly: InternalsVisibleTo("CloudChess.UnitTesting, PublicKey=" +
	"0024000004800000940000000602000000240000525341310004000001000100c383fea1f2a82f" +
	"1907c24e897c00a5f7bbae83552d599df7044ca30d90b0715c75644459758f43141ba431924fb6" +
	"7a5baa82ad63d47cecb639eaf9ec12f3ffdc4a831c188afdcaf12c1cfb01b0dda5ce2469d2488c" +
	"aabdc18d312787c234efef18d23dd56459632c644e3cb1c484c3728a7b67afe368c149f69b2ac1" +
	"8647f5e2")]

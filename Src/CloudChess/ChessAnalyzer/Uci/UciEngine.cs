﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;

using CloudChess.Analysis.Uci.Parsing;

namespace CloudChess.Analysis.Uci
{
	public class UciEngine : IUciEngine
	{
		private readonly string _path;
		private Process _process;
		private readonly IUciResponseParser _uciResponseParser;
		private IObservable<EventPattern<DataReceivedEventArgs>> _engineOutput;
		private IObservable<EventPattern<object>> _engineExited;

		public UciEngine(string path) : this(path, new UciResponseParser<UciResponseGrammar>())
		{
		}

		protected UciEngine(string path, IUciResponseParser uciResponseParser)
		{
			if (path == null) throw new ArgumentNullException("path");
			if (uciResponseParser == null) throw new ArgumentNullException("uciResponseParser");

			_path = path;
			_uciResponseParser = uciResponseParser;
		}

		public void Run()
		{
			if (_process != null) return;

			Process process = null;
			try
			{
				// Do not use object initialization beacause it violates the rule
				// CA2000 "Dispose object before losing scope".
				
				// ReSharper disable UseObjectOrCollectionInitializer
				process = new Process();
				process.StartInfo.FileName = _path;
				process.StartInfo.WorkingDirectory = Path.GetDirectoryName(_path);
				process.StartInfo.CreateNoWindow = true;
				process.StartInfo.RedirectStandardInput = true;
				process.StartInfo.RedirectStandardOutput = true;
				process.StartInfo.UseShellExecute = false;
				process.EnableRaisingEvents = true;
				// ReSharper restore UseObjectOrCollectionInitializer

				_engineOutput = Observable.FromEventPattern<DataReceivedEventArgs>(process, "OutputDataReceived");
				_engineExited = Observable.FromEventPattern(process, "Exited");
				_engineExited.Subscribe(_ => Dispose());

				try
				{
					process.Start();
				}
				catch (Win32Exception exception)
				{
					throw new UciEngineLoadException("Cannot load UCI engine.", exception);
				}
				process.BeginOutputReadLine();
				
				_process = process;
				process = null;
			}
			finally
			{
				if (process != null) process.Dispose();
			}
		}

		public IObservable<Unit> Closed
		{
			get { return _engineExited.Select(e => Unit.Default); }
		}

		public void SendCommand(IUciCommand command)
		{
			if (command == null) throw new ArgumentNullException("command");
			if (_process == null) throw new InvalidOperationException("The engine is disposed or not initialized yet.");
			_process.StandardInput.WriteLine(command.CommandText);
		}

		public IObservable<UciResponse> Responses
		{
			get
			{
				// TODO: Include errors (from stderr) into Responses

				if (_engineOutput == null) return Observable.Empty<UciResponse>();
				return _engineOutput.
					Where(response => response.EventArgs.Data != null).
					SelectMany(response => ParseUciEngineResponse(response.EventArgs.Data));
			}
		}


		private IEnumerable<UciResponse> ParseUciEngineResponse(string multilineResponse)
		{
			return multilineResponse.
				Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).
				SelectMany(line =>
					{
						try
						{
							return new[] {_uciResponseParser.Parse(line)};
						}
						catch (UciResponseParsingException)
						{
							return Enumerable.Empty<UciResponse>();
						}
					}
				);
		}

		#region IDisposable implementation

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		~UciEngine()
		{
			Dispose(false);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!disposing) return;
			if (_process == null) return;

			_process.Dispose();
			_process = null;
			_engineOutput = null;
		}

		#endregion
	}
}

﻿using System;
using System.Reactive;

namespace CloudChess.Analysis.Uci
{
	public interface IUciEngine : IDisposable
	{
		void Run();
		void SendCommand(IUciCommand command);
		IObservable<UciResponse> Responses { get; }
		IObservable<Unit> Closed { get; }
	}
}

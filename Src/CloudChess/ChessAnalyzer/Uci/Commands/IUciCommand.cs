﻿using System;

namespace CloudChess.Analysis.Uci
{
	public interface IUciCommand
	{
		string CommandText { get; }
	}

	public abstract class UciCommand : IUciCommand, IEquatable<IUciCommand>
	{
		public abstract string CommandText { get; }
		
		public bool Equals(IUciCommand other)
		{
			if (other == null) return false;
			return CommandText == other.CommandText;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			var other = obj as IUciCommand;
			return other != null && Equals(other);
		}

		public override int GetHashCode()
		{
			return CommandText.GetHashCode();
		}

		public static bool operator ==(UciCommand left, UciCommand right)
		{
			return Equals(left, right);
		}

		public static bool operator !=(UciCommand left, UciCommand right)
		{
			return !Equals(left, right);
		}
	}
}

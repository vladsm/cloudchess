﻿namespace CloudChess.Analysis.Uci
{
	internal sealed class PositionUciCommand : UciCommand
	{
		private readonly IUciPosition _position;

		public PositionUciCommand(IUciPosition position)
		{
			_position = position;
		}

		public override string CommandText
		{
			get { return "position " + _position.ToUciDescriptor(); }
		}
	}
}

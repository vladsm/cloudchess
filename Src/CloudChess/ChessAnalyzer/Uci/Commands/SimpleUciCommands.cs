﻿namespace CloudChess.Analysis.Uci
{
	internal sealed class InitUciCommand : UciCommand
	{
		public override string CommandText
		{
			get { return "uci"; }
		}
	}

	internal sealed class QuitUciCommand : UciCommand
	{
		public override string CommandText
		{
			get { return "quit"; }
		}
	}

	internal sealed class IsReadyUciCommand : UciCommand
	{
		public override string CommandText
		{
			get { return "isready"; }
		}
	}

	internal sealed class NewGameUciCommand : UciCommand
	{
		public override string CommandText
		{
			get { return "ucinewgame"; }
		}
	}
}

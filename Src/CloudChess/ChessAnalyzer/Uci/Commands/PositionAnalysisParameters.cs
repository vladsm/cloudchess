﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CloudChess.Analysis.Uci
{
	public sealed class PositionAnalysisParameters
	{
		public IEnumerable<CnMove> SearchMoves { get; set; }
		public bool Pondering { get; set; }
		public IAnalysisTimeRestrictions TimeRestrictions { get; set; }
		public int? MaxDepth { get; set; }
		public int? MaxNodes { get; set; }
		public int? MovesToSearchMate { get; set; }

		public string ToUciDescriptor()
		{
			var parts = new List<string>();
			
			if (Pondering) parts.Add("ponder");
			if (TimeRestrictions != null) parts.Add(TimeRestrictions.ToUciDescriptor());
			if (MovesToSearchMate.HasValue) parts.Add("mate " + MovesToSearchMate.Value);
			if (MaxDepth.HasValue) parts.Add("depth " + MaxDepth.Value);
			if (MaxNodes.HasValue) parts.Add("nodes " + MaxNodes.Value);

			var searchMoves = (SearchMoves ?? Enumerable.Empty<CnMove>()).Select(m => m.Notation).ToArray();
			if (searchMoves.Any())
			{
				parts.Add("searchmoves " + string.Join(" ", searchMoves));
			}

			return string.Join(" ", parts);
		}
	}


	public interface IAnalysisTimeRestrictions
	{
		string ToUciDescriptor();
	}


	public class InfiniteAnalysisTime : IAnalysisTimeRestrictions
	{
		public string ToUciDescriptor()
		{
			return "infinite";
		}
	}


	public class ExactAnalysisTime : IAnalysisTimeRestrictions
	{
		private readonly int _milliseconds;

		public ExactAnalysisTime(int milliseconds)
		{
			if (milliseconds <= 0)
			{
				throw new ArgumentOutOfRangeException(
					"milliseconds",
					milliseconds,
					"Analysis time should be greater than zero."
					);
			}
			_milliseconds = milliseconds;
		}

		public string ToUciDescriptor()
		{
			return "movetime " + _milliseconds;
		}
	}


	public class ClockTime : IAnalysisTimeRestrictions
	{
		private readonly int _whiteMilliseconds;
		private readonly int _blackMilliseconds;
		private readonly int _whiteIncrementMilliseconds;
		private readonly int _blackIncrementMilliseconds;
		private int _movesToNextTimeControl;

		public ClockTime(int whiteMilliseconds, int blackMilliseconds)
		{
			if (whiteMilliseconds <= 0)
			{
				throw new ArgumentOutOfRangeException(
					"whiteMilliseconds",
					whiteMilliseconds,
					"Clock time should be greater than zero."
					);
			}
			if (blackMilliseconds <= 0)
			{
				throw new ArgumentOutOfRangeException(
					"blackMilliseconds",
					blackMilliseconds,
					"Clock time should be greater than zero."
					);
			}
			_whiteMilliseconds = whiteMilliseconds;
			_blackMilliseconds = blackMilliseconds;
		}

		public ClockTime(
			int whiteMilliseconds,
			int blackMilliseconds,
			int whiteIncrementMilliseconds,
			int blackIncrementMilliseconds
			) :
			this(whiteMilliseconds, blackMilliseconds)
		{
			if (whiteIncrementMilliseconds <= 0)
			{
				throw new ArgumentOutOfRangeException(
					"whiteIncrementMilliseconds",
					whiteIncrementMilliseconds,
					"White increment should be greater than zero."
					);
			}
			if (blackIncrementMilliseconds <= 0)
			{
				throw new ArgumentOutOfRangeException(
					"blackIncrementMilliseconds",
					blackIncrementMilliseconds,
					"Black increment should be greater than zero."
					);
			}
			_whiteIncrementMilliseconds = whiteIncrementMilliseconds;
			_blackIncrementMilliseconds = blackIncrementMilliseconds;
		}

		public ClockTime(int whiteMilliseconds, int blackMilliseconds, int movesToNextTimeControl) :
			this(whiteMilliseconds, blackMilliseconds)
		{
			InitializeMovesToNextTimeControl(movesToNextTimeControl);
		}

		public ClockTime(
			int whiteMilliseconds,
			int blackMilliseconds,
			int whiteIncrementMilliseconds,
			int blackIncrementMilliseconds,
			int movesToNextTimeControl
			) :
			this(whiteMilliseconds, blackMilliseconds, whiteIncrementMilliseconds, blackIncrementMilliseconds)
		{
			InitializeMovesToNextTimeControl(movesToNextTimeControl);
		}

		private void InitializeMovesToNextTimeControl(int movesToNextTimeControl)
		{
			if (movesToNextTimeControl <= 0)
			{
				throw new ArgumentOutOfRangeException(
					"movesToNextTimeControl",
					movesToNextTimeControl,
					"Number of moves to next time control should be greater than zero."
					);
			}
			_movesToNextTimeControl = movesToNextTimeControl;
		}

		public string ToUciDescriptor()
		{
			var parts = new List<string> { "wtime " + _whiteMilliseconds + " btime " + _blackMilliseconds };
			if (_whiteIncrementMilliseconds > 0 && _blackIncrementMilliseconds > 0)
			{
				parts.Add("winc " + _whiteIncrementMilliseconds + " binc " + _blackIncrementMilliseconds);
			}
			if (_movesToNextTimeControl > 0)
			{
				parts.Add("movestogo " + _movesToNextTimeControl);
			}
			return string.Join(" ", parts);
		}
	}
}

﻿using System;

namespace CloudChess.Analysis.Uci
{
	internal sealed class GoUciCommand : UciCommand
	{
		private readonly PositionAnalysisParameters _positionAnalysisParameters;

		public GoUciCommand(PositionAnalysisParameters positionAnalysisParameters)
		{
			if (positionAnalysisParameters == null) throw new ArgumentNullException("positionAnalysisParameters");
			_positionAnalysisParameters = positionAnalysisParameters;
		}

		public override string CommandText
		{
			get { return "go " + _positionAnalysisParameters.ToUciDescriptor(); }
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CloudChess.Analysis.Uci
{
	/// <summary>
	/// Represents the position in a game to be sent to UCI-engine.
	/// </summary>
	public interface IUciPosition
	{
		/// <summary>
		/// Gets text descriptor for position to be used in "position" UCI-command.
		/// </summary>
		string ToUciDescriptor();
	}


	/// <summary>
	/// Represents UCI position description based on source position description and following moves.
	/// </summary>
	public abstract class SourceAndMovesUciPosition : IUciPosition
	{
		private readonly List<CnMove> _moves;

		protected abstract string SourcePositionRepresentation { get; }

		protected IEnumerable<CnMove> Moves
		{
			get { return _moves; }
		}

		protected SourceAndMovesUciPosition(IEnumerable<CnMove> moves)
		{
			if (moves == null) throw new ArgumentNullException("moves");
			_moves = new List<CnMove>(moves);
		}

		public string ToUciDescriptor()
		{
			string sourcePosition = SourcePositionRepresentation;
			return Moves.Any() ? 
				sourcePosition + " moves " + String.Join(" ", Moves.Select(m => m.Notation)) :
				sourcePosition;
		}
	}


	/// <summary>
	/// Represents the UCI position originated from start position.
	/// </summary>
	public sealed class StartUciPosition : SourceAndMovesUciPosition
	{
		public StartUciPosition() : this(Enumerable.Empty<CnMove>())
		{
		}

		public StartUciPosition(IEnumerable<CnMove> moves) : base(moves)
		{
		}

		protected override string SourcePositionRepresentation
		{
			get { return "startpos"; }
		}
	}


	/// <summary>
	/// Represents the UCI position originated from specified FEN-position (Forsyth–Edwards Notation).
	/// </summary>
	public sealed class FenUciPosition : SourceAndMovesUciPosition
	{
		private readonly string _fen;

		public FenUciPosition(string fen) : this(fen, Enumerable.Empty<CnMove>())
		{
		}

		public FenUciPosition(string fen, IEnumerable<CnMove> moves) : base(moves)
		{
			if (fen == null) throw new ArgumentNullException("fen");
			_fen = fen;
		}

		protected override string SourcePositionRepresentation
		{
			get { return "fen " + _fen; }
		}
	}
}

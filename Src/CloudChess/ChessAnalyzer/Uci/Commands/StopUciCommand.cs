﻿namespace CloudChess.Analysis.Uci
{
	internal class StopUciCommand : UciCommand
	{
		public override string CommandText
		{
			get { return "stop"; }
		}
	}
}

﻿using System;

namespace CloudChess.Analysis.Uci
{
	internal class SetOptionUciCommand : UciCommand
	{
		private readonly string _optionName;
		private readonly string _optionValue;

		public SetOptionUciCommand(string optionName, string optionValue)
		{
			if (optionName == null) throw new ArgumentNullException("optionName");
			_optionName = optionName;
			_optionValue = optionValue;
		}

		public override string CommandText
		{
			get
			{
				return "setoption name " + _optionName +
					(_optionValue == null ? string.Empty : " value " + (_optionValue.Length > 0 ? _optionValue : "<empty>"));
			}
		}
	}
}

﻿namespace CloudChess.Analysis.Uci
{
	internal static class UciCommands
	{
		public static IUciCommand InitUci
		{
			get { return new InitUciCommand(); }
		}

		public static IUciCommand Quit
		{
			get { return new QuitUciCommand(); }
		}

		public static IUciCommand IsReady
		{
			get { return new IsReadyUciCommand(); }
		}

		public static IUciCommand NewGame
		{
			get { return new NewGameUciCommand(); }
		}

		public static IUciCommand Position(IUciPosition position)
		{
			return new PositionUciCommand(position);
		}

		public static IUciCommand Go(PositionAnalysisParameters positionAnalysisParameters)
		{
			return new GoUciCommand(positionAnalysisParameters);
		}

		public static IUciCommand Stop
		{
			get { return new StopUciCommand(); }
		}

		public static IUciCommand SetOption(string optionName, string optionValue)
		{
			return new SetOptionUciCommand(optionName, optionValue);
		}
	}
}

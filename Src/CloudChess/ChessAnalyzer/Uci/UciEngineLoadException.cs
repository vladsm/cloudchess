﻿using System;
using System.Runtime.Serialization;

namespace CloudChess.Analysis.Uci
{
	[Serializable]
	public class UciEngineLoadException : Exception
	{
		public UciEngineLoadException()
		{
		}

		public UciEngineLoadException(string message)
			: base(message)
		{
		}

		public UciEngineLoadException(string message, Exception inner)
			: base(message, inner)
		{
		}

		protected UciEngineLoadException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}
}

﻿using System;
using System.Linq;

namespace CloudChess.Analysis.Uci
{
	internal sealed class InfoUciResponse : UciResponse, IEquatable<InfoUciResponse>
	{
		/// <summary>
		/// Gets or sets a search depth in plies.
		/// </summary>
		public int? Depth { get; set; }

		/// <summary>
		/// Gets or sets a selective search depth in plies.
		/// </summary>
		public int? SelectiveDepth { get; set; }

		/// <summary>
		/// Gets or sets the time searched in milliseconds.
		/// </summary>
		public int? Time { get; set; }

		/// <summary>
		/// Gets or sets the nodes searched.
		/// </summary>
		public int? Nodes { get; set; }

		/// <summary>
		/// Gets or sets a principal variation produced by UCI-engine.
		/// </summary>
		public CnMove[] Pv { get; set; }

		/// <summary>
		/// Gets or sets a principal variation index for the multipv mode.
		/// </summary>
		public int? MultiPv { get; set; }

		/// <summary>
		/// Gets or sets a currently searching move.
		/// </summary>
		public CnMove CurrentMove { get; set; }

		/// <summary>
		/// Gets or set currently searching move number (1-based).
		/// </summary>
		public int? CurrentMoveNumber { get; set; }

		/// <summary>
		/// Gets or sets a hash fullness value in permill.
		/// </summary>
		public int? HashFull { get; set; }

		/// <summary>
		/// Gets or sets a number of nodes searched per second.
		/// </summary>
		public int? NodesPerSecond { get; set; }

		/// <summary>
		/// Gets or sets a number of positions found inthe endgame table.
		/// </summary>
		public int? TbHits { get; set; }

		/// <summary>
		/// Gets or sets a CPU usage in permill.
		/// </summary>
		public int? CpuLoad { get; set; }

		/// <summary>
		/// Gets or sets a position estimation;
		/// </summary>
		public PositionEstimation PositionEstimation { get; set; }

		/// <summary>
		/// Gets or sets a text message.
		/// </summary>
		public string Message { get; set; }

		/// <summary>
		/// Gets or sets a refutation.
		/// </summary>
		public CnMove[] Refutation { get; set; }

		/// <summary>
		/// Gets or set currently calculating line.
		/// </summary>
		public CurrentCalculatingLine CurrentLine { get; set; }


		#region Equality overridings

		public bool Equals(InfoUciResponse other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;

			return Depth == other.Depth &&
				SelectiveDepth == other.SelectiveDepth &&
				Time == other.Time &&
				Nodes == other.Nodes &&
				(Pv == null && other.Pv == null || Pv != null && Pv.SequenceEqual(other.Pv)) &&
				MultiPv == other.MultiPv &&
				CurrentMove == other.CurrentMove &&
				CurrentMoveNumber == other.CurrentMoveNumber &&
				HashFull == other.HashFull &&
				NodesPerSecond == other.NodesPerSecond &&
				TbHits == other.TbHits &&
				CpuLoad == other.CpuLoad &&
				Equals(PositionEstimation, other.PositionEstimation) &&
				Message == other.Message &&
				(Refutation == null && other.Refutation == null || Refutation != null && Refutation.SequenceEqual(other.Refutation)) &&
				(CurrentLine == null && other.CurrentLine == null ||
					CurrentLine != null && other.CurrentLine != null && 
						CurrentLine.CpuIndex == other.CurrentLine.CpuIndex &&
						CurrentLine.Moves.SequenceEqual(other.CurrentLine.Moves)
					);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			var typedObj = obj as InfoUciResponse;
			return typedObj != null && Equals(typedObj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				const int shift = 397;
				int hashCode = Depth.GetHashCode();
				hashCode = (hashCode*shift) ^ SelectiveDepth.GetHashCode();
				hashCode = (hashCode*shift) ^ Time.GetHashCode();
				hashCode = (hashCode*shift) ^ Nodes.GetHashCode();
				hashCode = (hashCode*shift) ^ (Pv == null ? 0 : Pv.GetHashCode());
				hashCode = (hashCode*shift) ^ MultiPv.GetHashCode();
				hashCode = (hashCode*shift) ^ (CurrentMove == null ? 0 : CurrentMove.GetHashCode());
				hashCode = (hashCode*shift) ^ CurrentMoveNumber.GetHashCode();
				hashCode = (hashCode*shift) ^ HashFull.GetHashCode();
				hashCode = (hashCode*shift) ^ NodesPerSecond.GetHashCode();
				hashCode = (hashCode*shift) ^ TbHits.GetHashCode();
				hashCode = (hashCode*shift) ^ CpuLoad.GetHashCode();
				hashCode = (hashCode*shift) ^ (PositionEstimation == null ? 0 : PositionEstimation.GetHashCode());
				hashCode = (hashCode*shift) ^ Message.GetHashCode();
				hashCode = (hashCode*shift) ^ (Refutation == null ? 0 : Refutation.GetHashCode());
				if (CurrentLine == null)
				{
					hashCode = hashCode*shift;
				}
				else
				{
					hashCode = (hashCode * shift) ^ CurrentLine.CpuIndex.GetHashCode();
					hashCode = (hashCode * shift) ^ CurrentLine.Moves.GetHashCode();
				}
				return hashCode;
			}
		}

		#endregion
	}
}

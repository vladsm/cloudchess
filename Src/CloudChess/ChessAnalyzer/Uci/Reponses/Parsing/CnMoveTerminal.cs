﻿using System;
using System.Collections.Generic;

using Irony.Parsing;

namespace CloudChess.Analysis.Uci.Parsing
{
	public class CnMoveTerminal : Terminal
	{
		public CnMoveTerminal(string name) : base(name)
		{
			AstConfig.NodeType = typeof(MoveNode);
		}

		public override IList<string> GetFirsts()
		{
			return new[] {"a", "b", "c", "d", "e", "f", "g", "h"};
		}

		public override Token TryMatch(ParsingContext context, ISourceStream source)
		{
			if (source == null) throw new ArgumentNullException("source");

			int previewPosition = source.PreviewPosition;

			var moveCandidate = source.Text.Substring(
				previewPosition,
				Math.Min(5, source.Text.Length - previewPosition)
				);
			if (moveCandidate.Length < 4) return null;
			if (!IsVerticalCoordinate(moveCandidate[0])) return null;
			if (!IsHorizontalCoordinate(moveCandidate[1])) return null;
			if (!IsVerticalCoordinate(moveCandidate[2])) return null;
			if (!IsHorizontalCoordinate(moveCandidate[3])) return null;

			if (moveCandidate.Length < 5)
			{
				source.PreviewPosition += 4;
			}
			else if (IsPromotion(moveCandidate[4]))
			{
				source.PreviewPosition += 5;
			}
			else
			{
				if (!Grammar.IsWhitespaceOrDelimiter(moveCandidate[4])) return null;
				source.PreviewPosition += 4;
			}

			return source.CreateToken(OutputTerminal);
		}

		private static bool IsVerticalCoordinate(char ch)
		{
			switch (ch)
			{
				case 'a':
				case 'b':
				case 'c':
				case 'd':
				case 'e':
				case 'f':
				case 'g':
				case 'h':
					return true;
			}
			return false;
		}

		private static bool IsHorizontalCoordinate(char ch)
		{
			switch (ch)
			{
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
					return true;
			}
			return false;
		}

		private static bool IsPromotion(char ch)
		{
			switch (ch)
			{
				case 'r':
				case 'n':
				case 'b':
				case 'q':
					return true;
			}
			return false;
		}
	}
}

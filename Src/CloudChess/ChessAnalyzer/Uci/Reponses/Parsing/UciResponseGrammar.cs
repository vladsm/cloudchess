﻿using System;
using System.Collections.Generic;

using Irony.Parsing;

namespace CloudChess.Analysis.Uci.Parsing
{
	/// <summary>
	/// Represents the grammar for the UCI response.
	/// </summary>
	[Language("UCI response")]
	public class UciResponseGrammar : Grammar
	{
		protected CnMoveTerminal MoveTerminal { get; private set; }
		protected NumberLiteral UnsignedIntegerTerminal { get; private set; }
		protected NumberLiteral IntegerTerminal { get; private set; }
		protected FreeTextLiteral FreeTextTerminal { get; private set; }

		private readonly NonTerminal _infoItem;
		protected BnfExpression InfoItemRule
		{
			get { return _infoItem.Rule; }
			set { _infoItem.Rule = value; }
		}

		public UciResponseGrammar() : base(true)
		{
			// Terminals
			var uciok = ToTerm("uciok");
			var readyok = ToTerm("readyok");
			var bestmove = ToTerm("bestmove");
			var ponder = ToTerm("ponder");
			var info = ToTerm("info");
			var depth = ToTerm("depth");
			var seldepth = ToTerm("seldepth");
			var time = ToTerm("time");
			var nodes = ToTerm("nodes");
			var pv = ToTerm("pv");
			var multipv = ToTerm("multipv");
			var currmove = ToTerm("currmove");
			var currmovenumber = ToTerm("currmovenumber");
			var hashfull = ToTerm("hashfull");
			var nps = ToTerm("nps");
			var tbhits = ToTerm("tbhits");
			var cpuload = ToTerm("cpuload");
			var score = ToTerm("score");
			var cp = ToTerm("cp");
			var mate = ToTerm("mate");
			var lowerbound = ToTerm("lowerbound");
			var upperbound = ToTerm("upperbound");
			var stringKeyword = ToTerm("string");
			var refutation = ToTerm("refutation");
			var currline = ToTerm("currline");

			MoveTerminal = new CnMoveTerminal("Move");
			UnsignedIntegerTerminal = new NumberLiteral("UnsignedInteger", NumberOptions.IntOnly, typeof(IntegerNode))
			{
				DefaultIntTypes = new[] {TypeCode.UInt32}
			};
			IntegerTerminal = new NumberLiteral("Integer", NumberOptions.IntOnly | NumberOptions.AllowSign, typeof(IntegerNode))
			{
				DefaultIntTypes = new[] {TypeCode.Int32}
			};
			FreeTextTerminal = new FreeTextLiteral("FreeText", FreeTextOptions.AllowEof)
			{
				AstConfig = {NodeType = typeof(StringNode)}
			};

			// Non-Terminals
			var response = new NonTerminal("Response");
			var movesList = new NonTerminal("MovesList", typeof(MovesListNode));
			var uciokResponse = new NonTerminal("UciOk", typeof(UciOkResponseNode));
			var readyokResponse = new NonTerminal("ReadyOk", typeof(ReadyOkUciResponseNode));
			var bestmoveResponse = new NonTerminal("BestMove", typeof(BestMoveResponseNode));
			var infoResponse = new NonTerminal("Info", typeof(InfoResponseNode));
			var infoItems = new NonTerminal("InfoItems" ,typeof(InfoItemsNode));
			_infoItem = new NonTerminal("InfoItem");
			var infoDepthItem = new NonTerminal("InfoDepthItem", typeof(InfoDepthNode));
			var infoSelectiveDepthItem = new NonTerminal("InfoSelectiveDepthItem", typeof(InfoSelectiveDepthNode));
			var infoTimeItem = new NonTerminal("InfoTimeItem", typeof(InfoTimeNode));
			var infoNodesItem = new NonTerminal("InfoNodesItem", typeof(InfoNodesNode));
			var infoPvItem = new NonTerminal("InfoPvItem", typeof(InfoPvNode));
			var infoMultiPvItem = new NonTerminal("InfoMultiPvItem", typeof(InfoMultiplePvNode));
			var infoCurrentMoveItem = new NonTerminal("InfoCurrentMoveItem", typeof(InfoCurrentMoveNode));
			var infoCurrentMoveNumberItem = new NonTerminal("InfoCurrentMoveNumberItem", typeof(InfoCurrentMoveNumberNode));
			var infoHashFullItem = new NonTerminal("InfoHashFullItem", typeof(InfoHashFullNode));
			var infoNodesPerSecondItem = new NonTerminal("InfoNodesPerSecondItem", typeof(InfoNodesPerSecondNode));
			var infoTbHitsItem = new NonTerminal("InfoTbHitsItem", typeof(InfoTableHitsNode));
			var infoCpuLoadItem = new NonTerminal("InfoCpuLoadItem", typeof(InfoCpuLoadNode));
			var infoScoreItem = new NonTerminal("InfoScoreItem", typeof(InfoScoreNode));
			var positionScore = new NonTerminal("CpEstimation");
			var exactPositionScore = new NonTerminal("CpExactEstimation", typeof(ExactPositionScoreNode));
			var lowerBoundPositionScore = new NonTerminal("CpLowerBoundEstimation", typeof(LowerBoundPositionScoreNode));
			var upperBoundPositionScore = new NonTerminal("CpUpperBoundEstimation", typeof(UpperBoundPositionScoreNode));
			var matePrediction = new NonTerminal("MatePrediction", typeof(MatePredictionNode));
			var infoStringItem = new NonTerminal("InfoStringItem", typeof(InfoStringNode));
			var infoRefutationItem = new NonTerminal("InfoRefutationItem", typeof(InfoRefutationNode));
			var infoCurrentLineItem = new NonTerminal("InfoCurrentLineItem", typeof(InfoCurrentLineNode));

			// BNF rules
			response.Rule = uciokResponse | readyokResponse | bestmoveResponse | infoResponse;
			uciokResponse.Rule = uciok;
			readyokResponse.Rule = readyok;
			bestmoveResponse.Rule =
				bestmove + MoveTerminal |
				bestmove + MoveTerminal + ponder + MoveTerminal;

			infoResponse.Rule = info + infoItems;
			infoItems.Rule = MakePlusRule(infoItems, _infoItem);
			InfoItemRule = 
				infoDepthItem | infoSelectiveDepthItem | infoTimeItem | infoNodesItem | infoPvItem |
				infoMultiPvItem | infoCurrentMoveItem | infoCurrentMoveNumberItem | infoHashFullItem |
				infoNodesPerSecondItem | infoTbHitsItem | infoCpuLoadItem | infoScoreItem | infoStringItem |
				infoRefutationItem | infoCurrentLineItem;
			infoDepthItem.Rule = depth + UnsignedIntegerTerminal;
			infoSelectiveDepthItem.Rule = seldepth + UnsignedIntegerTerminal;
			infoTimeItem.Rule = time + UnsignedIntegerTerminal;
			infoNodesItem.Rule = nodes + UnsignedIntegerTerminal;
			infoPvItem.Rule = pv + movesList;
			infoMultiPvItem.Rule = multipv + UnsignedIntegerTerminal;
			infoCurrentMoveItem.Rule = currmove + MoveTerminal;
			infoCurrentMoveNumberItem.Rule = currmovenumber + UnsignedIntegerTerminal;
			infoHashFullItem.Rule = hashfull + UnsignedIntegerTerminal;
			infoNodesPerSecondItem.Rule = nps + UnsignedIntegerTerminal;
			infoTbHitsItem.Rule = tbhits + UnsignedIntegerTerminal;
			infoCpuLoadItem.Rule = cpuload + UnsignedIntegerTerminal;
			infoScoreItem.Rule = score + cp + positionScore | score + mate + matePrediction;
			positionScore.Rule = exactPositionScore | lowerBoundPositionScore | upperBoundPositionScore;
			exactPositionScore.Rule = IntegerTerminal;
			lowerBoundPositionScore.Rule = IntegerTerminal + lowerbound;
			upperBoundPositionScore.Rule = IntegerTerminal + upperbound;
			matePrediction.Rule = IntegerTerminal;
			infoStringItem.Rule = stringKeyword + FreeTextTerminal;
			infoRefutationItem.Rule = refutation + movesList;
			infoCurrentLineItem.Rule = currline + UnsignedIntegerTerminal + movesList | currline + movesList;

			movesList.Rule = MakePlusRule(movesList, MoveTerminal);
			Root = response;

			// Other grammar setups
			MarkTransient(response, _infoItem, positionScore);
			MarkPunctuation(
				uciok, readyok, bestmove, ponder, info, depth, seldepth, time, nodes, pv, multipv,
				currmove, currmovenumber, hashfull, nps, tbhits, cpuload, score, cp, mate, lowerbound,
				upperbound, stringKeyword, refutation, currline
				);
			
			// Language flags
			LanguageFlags = LanguageFlags.CreateAst;
		}
	}
}

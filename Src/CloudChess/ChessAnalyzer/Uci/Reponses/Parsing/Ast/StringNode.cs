﻿using System;

using Irony.Ast;
using Irony.Interpreter.Ast;
using Irony.Parsing;

namespace CloudChess.Analysis.Uci.Parsing
{
	public sealed class StringNode : AstNode, IValueProviderNode
	{
		public string Value { get; private set; }

		object IValueProviderNode.NodeValue
		{
			get { return Value; }
		}

		public override void Init(AstContext context, ParseTreeNode treeNode)
		{
			if (treeNode == null) throw new ArgumentNullException("treeNode");
			base.Init(context, treeNode);
			Value = treeNode.FindTokenAndGetText();
		}
	}
}

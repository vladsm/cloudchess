using System;
using System.Linq;

using Irony.Interpreter.Ast;

namespace CloudChess.Analysis.Uci.Parsing
{
	public abstract class MovesListInfoItemNode : InfoItemNode
	{
		protected override object ExtractValue(AstNode[] valueNodes)
		{
			if (valueNodes == null) throw new ArgumentNullException("valueNodes");
			if (!valueNodes.Any()) throw new ArgumentException("Unexpected empty array.", "valueNodes");

			return valueNodes[0].ChildNodes.
				Cast<MoveNode>().
				Select(moveNode => moveNode.Move).
				ToArray();
		}
	}
}

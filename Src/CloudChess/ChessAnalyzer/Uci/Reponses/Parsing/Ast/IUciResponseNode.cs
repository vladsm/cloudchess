﻿namespace CloudChess.Analysis.Uci.Parsing
{
	public interface IUciResponseNode
	{
		UciResponse GetUciResponse();
	}
}

using System;

using Irony.Interpreter.Ast;

namespace CloudChess.Analysis.Uci.Parsing
{
	public sealed class InfoScoreNode : InfoItemNode
	{
		protected override object ExtractValue(AstNode[] valueNodes)
		{
			if (valueNodes == null) throw new ArgumentNullException("valueNodes");

			return ((PositionEstimationNode)valueNodes[0]).PositionEstimation;
		}

		internal override void BuildPart(InfoUciResponse infoResponse)
		{
			infoResponse.PositionEstimation = (PositionEstimation)Value;
		}
	}
}

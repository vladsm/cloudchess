﻿using System;

using Irony.Interpreter.Ast;

namespace CloudChess.Analysis.Uci.Parsing
{
	public sealed class InfoCurrentLineNode : MovesListInfoItemNode
	{
		protected override object ExtractValue(AstNode[] valueNodes)
		{
			if (valueNodes == null) throw new ArgumentNullException("valueNodes");

			return valueNodes.Length > 1 ? 
				new CurrentCalculatingLine(
					((IntegerNode)valueNodes[0]).Value,
					(CnMove[])base.ExtractValue(new[] {valueNodes[1]})
					) :
				new CurrentCalculatingLine((CnMove[])base.ExtractValue(valueNodes));
		}

		internal override void BuildPart(InfoUciResponse infoResponse)
		{
			infoResponse.CurrentLine = (CurrentCalculatingLine)Value;
		}
	}
}

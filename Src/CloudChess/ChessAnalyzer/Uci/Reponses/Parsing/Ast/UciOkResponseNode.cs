﻿using Irony.Interpreter.Ast;

namespace CloudChess.Analysis.Uci.Parsing
{
	public class UciOkResponseNode : AstNode, IUciResponseNode
	{
		public UciResponse GetUciResponse()
		{
			return new UciOkResponse();
		}
	}
}

﻿using System;

using Irony.Ast;
using Irony.Interpreter.Ast;
using Irony.Parsing;

namespace CloudChess.Analysis.Uci.Parsing
{
	public abstract class PositionEstimationNode : AstNode
	{
		public PositionEstimation PositionEstimation { get; protected set; }
	}


	public sealed class MatePredictionNode : PositionEstimationNode
	{
		public override void Init(AstContext context, ParseTreeNode treeNode)
		{
			if (treeNode == null) throw new ArgumentNullException("treeNode");
			base.Init(context, treeNode);
			PositionEstimation = new MatePrediction(((IntegerNode)treeNode.ChildNodes[0].AstNode).Value);
		}
	}


	public abstract class PositionScoreNode : PositionEstimationNode
	{
		protected abstract PositionScoreType PositionScoreType { get; }
	
		public override void Init(AstContext context, ParseTreeNode treeNode)
		{
			if (treeNode == null) throw new ArgumentNullException("treeNode");
			base.Init(context, treeNode);
			PositionEstimation = new PositionScore(
				((IntegerNode)treeNode.ChildNodes[0].AstNode).Value,
				PositionScoreType
				);
		}
	}

	public sealed class ExactPositionScoreNode : PositionScoreNode
	{
		protected override PositionScoreType PositionScoreType
		{
			get { return PositionScoreType.Exact; }
		}
	}

	public sealed class LowerBoundPositionScoreNode : PositionScoreNode
	{
		protected override PositionScoreType PositionScoreType
		{
			get { return PositionScoreType.LowerBound; }
		}
	}

	public sealed class UpperBoundPositionScoreNode : PositionScoreNode
	{
		protected override PositionScoreType PositionScoreType
		{
			get { return PositionScoreType.UpperBound; }
		}
	}
}

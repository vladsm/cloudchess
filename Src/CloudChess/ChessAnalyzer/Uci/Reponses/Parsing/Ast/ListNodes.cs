﻿using System;

using Irony.Ast;
using Irony.Interpreter.Ast;
using Irony.Parsing;

namespace CloudChess.Analysis.Uci.Parsing
{
	public abstract class ListBaseNode : AstNode
	{
		public abstract string ItemRoleName { get; }

		public override void Init(AstContext context, ParseTreeNode treeNode)
		{
			if (treeNode == null) throw new ArgumentNullException("treeNode");
			base.Init(context, treeNode);

			foreach (var child in treeNode.ChildNodes)
			{
				AddChild(ItemRoleName, child);
			}
		}
	}


	public sealed class InfoItemsNode : ListBaseNode
	{
		public override string ItemRoleName
		{
			get { return "InfoItem"; }
		}
	}


	public sealed class MovesListNode : ListBaseNode
	{
		public override string ItemRoleName
		{
			get { return "Move"; }
		}
	}
}

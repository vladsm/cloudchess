﻿namespace CloudChess.Analysis.Uci.Parsing
{
	public sealed class InfoDepthNode : InfoItemNode
	{
		internal override void BuildPart(InfoUciResponse infoResponse)
		{
			infoResponse.Depth = (int)Value;
		}
	}

	public sealed class InfoSelectiveDepthNode : InfoItemNode
	{
		internal override void BuildPart(InfoUciResponse infoResponse)
		{
			infoResponse.SelectiveDepth = (int)Value;
		}
	}

	public sealed class InfoTimeNode : InfoItemNode
	{
		internal override void BuildPart(InfoUciResponse infoResponse)
		{
			infoResponse.Time = (int)Value;
		}
	}

	public sealed class InfoNodesNode : InfoItemNode
	{
		internal override void BuildPart(InfoUciResponse infoResponse)
		{
			infoResponse.Nodes = (int)Value;
		}
	}

	public sealed class InfoPvNode : MovesListInfoItemNode
	{
		internal override void BuildPart(InfoUciResponse infoResponse)
		{
			infoResponse.Pv = (CnMove[])Value;
		}
	}

	public sealed class InfoMultiplePvNode : InfoItemNode
	{
		internal override void BuildPart(InfoUciResponse infoResponse)
		{
			infoResponse.MultiPv = (int)Value;
		}
	}
	
	public sealed class InfoCurrentMoveNode : InfoItemNode
	{
		internal override void BuildPart(InfoUciResponse infoResponse)
		{
			infoResponse.CurrentMove = (CnMove)Value;
		}
	}
	
	public sealed class InfoCurrentMoveNumberNode : InfoItemNode
	{
		internal override void BuildPart(InfoUciResponse infoResponse)
		{
			infoResponse.CurrentMoveNumber = (int)Value;
		}
	}

	public sealed class InfoHashFullNode : InfoItemNode
	{
		internal override void BuildPart(InfoUciResponse infoResponse)
		{
			infoResponse.HashFull = (int)Value;
		}
	}

	public sealed class InfoNodesPerSecondNode : InfoItemNode
	{
		internal override void BuildPart(InfoUciResponse infoResponse)
		{
			infoResponse.NodesPerSecond = (int)Value;
		}
	}

	public sealed class InfoTableHitsNode : InfoItemNode
	{
		internal override void BuildPart(InfoUciResponse infoResponse)
		{
			infoResponse.TbHits = (int)Value;
		}
	}

	public sealed class InfoCpuLoadNode : InfoItemNode
	{
		internal override void BuildPart(InfoUciResponse infoResponse)
		{
			infoResponse.CpuLoad = (int)Value;
		}
	}

	public sealed class InfoStringNode : InfoItemNode
	{
		internal override void BuildPart(InfoUciResponse infoResponse)
		{
			infoResponse.Message = (string)Value;
		}
	}

	public sealed class InfoRefutationNode : MovesListInfoItemNode
	{
		internal override void BuildPart(InfoUciResponse infoResponse)
		{
			infoResponse.Refutation = (CnMove[])Value;
		}
	}
}

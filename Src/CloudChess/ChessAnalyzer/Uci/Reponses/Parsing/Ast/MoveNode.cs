using System;

using Irony.Ast;
using Irony.Interpreter.Ast;
using Irony.Parsing;

namespace CloudChess.Analysis.Uci.Parsing
{
	public class MoveNode : AstNode, IValueProviderNode
	{
		public CnMove Move { get; private set; }

		public override void Init(AstContext context, ParseTreeNode treeNode)
		{
			if (treeNode == null) throw new ArgumentNullException("treeNode");
			base.Init(context, treeNode);
			Move = new CnMove(treeNode.FindTokenAndGetText());
		}

		public object NodeValue
		{
			get { return Move; }
		}
	}
}

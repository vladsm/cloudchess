﻿using Irony.Interpreter.Ast;

namespace CloudChess.Analysis.Uci.Parsing
{
	public class ReadyOkUciResponseNode : AstNode, IUciResponseNode
	{
		public UciResponse GetUciResponse()
		{
			return new ReadyOkUciResponse();
		}
	}
}

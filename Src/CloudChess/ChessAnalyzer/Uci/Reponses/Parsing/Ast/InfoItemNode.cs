﻿using System;
using System.Linq;

using Irony.Ast;
using Irony.Interpreter.Ast;
using Irony.Parsing;

namespace CloudChess.Analysis.Uci.Parsing
{
	/// <summary>
	/// Represents AST-node for the item of the information sent by UCI-engine in "info" response.
	/// </summary>
	public abstract class InfoItemNode : AstNode
	{
		protected object Value { get; private set; }

		public override void Init(AstContext context, ParseTreeNode treeNode)
		{
			if (treeNode == null) throw new ArgumentNullException("treeNode");

			base.Init(context, treeNode);
			Value = ExtractValue(treeNode.ChildNodes.Select((n, i) => AddChild("Value" + i, n)).ToArray());
		}

		protected virtual object ExtractValue(AstNode[] valueNodes)
		{
			if (valueNodes == null) throw new ArgumentNullException("valueNodes");
			return ((IValueProviderNode)valueNodes[0]).NodeValue;
		}

		internal abstract void BuildPart(InfoUciResponse infoResponse);
	}


	/// <summary>
	/// Represents AST-node for the item of the information sent by UCI-engine in "info" response which is
	/// supposed to be skipped while parsing.
	/// </summary>
	public class InfoSkipNode : InfoItemNode
	{
		internal override void BuildPart(InfoUciResponse infoResponse)
		{
			// do nothing
		}
	}
}

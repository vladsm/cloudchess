﻿using System;

using Irony.Ast;
using Irony.Interpreter.Ast;
using Irony.Parsing;

namespace CloudChess.Analysis.Uci.Parsing
{
	public sealed class InfoResponseNode : AstNode, IUciResponseNode
	{
		public override void Init(AstContext context, ParseTreeNode treeNode)
		{
			if (treeNode == null) throw new ArgumentNullException("treeNode");

			base.Init(context, treeNode);
			AddChild("InfoItems", treeNode.ChildNodes[0]);
		}

		public UciResponse GetUciResponse()
		{
			var result = new InfoUciResponse();
			foreach (InfoItemNode infoItemNode in ChildNodes[0].ChildNodes)
			{
				infoItemNode.BuildPart(result);
			}
			return result;
		}
	}
}

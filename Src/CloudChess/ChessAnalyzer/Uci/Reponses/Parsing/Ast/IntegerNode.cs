﻿using System;

using Irony.Ast;
using Irony.Interpreter.Ast;
using Irony.Parsing;

namespace CloudChess.Analysis.Uci.Parsing
{
	public sealed class IntegerNode : AstNode, IValueProviderNode
	{
		public int Value { get; private set; }

		object IValueProviderNode.NodeValue
		{
			get { return Value; }
		}

		public override void Init(AstContext context, ParseTreeNode treeNode)
		{
			if (treeNode == null) throw new ArgumentNullException("treeNode");

			base.Init(context, treeNode);
			if (treeNode.Term.Name == "UnsignedInteger")
			{
				Value = checked((int)(uint)treeNode.Token.Value);
			}
			else
			{
				Value = checked((int)treeNode.Token.Value);
			}
		}
	}
}

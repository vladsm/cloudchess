﻿using System;

using Irony.Ast;
using Irony.Interpreter.Ast;
using Irony.Parsing;

namespace CloudChess.Analysis.Uci.Parsing
{
	public class BestMoveResponseNode : AstNode, IUciResponseNode
	{
		private MoveNode _move;
		private MoveNode _ponder;

		public override void Init(AstContext context, ParseTreeNode treeNode)
		{
			if (treeNode == null) throw new ArgumentNullException("treeNode");
			base.Init(context, treeNode);
			_move = (MoveNode)AddChild("Move", treeNode.ChildNodes[0]);
			if (treeNode.ChildNodes.Count > 1)
			{
				_ponder = (MoveNode)AddChild("Ponder", treeNode.ChildNodes[1]);
			}
		}

		public UciResponse GetUciResponse()
		{
			return _ponder == null ?
				new BestMoveUciResponse(_move.Move) :
				new BestMoveUciResponse(_move.Move, _ponder.Move);
		}
	}
}

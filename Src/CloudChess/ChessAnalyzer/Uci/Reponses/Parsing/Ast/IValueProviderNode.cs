﻿namespace CloudChess.Analysis.Uci.Parsing
{
	/// <summary>
	/// Describes AST-node that providing value.
	/// </summary>
	internal interface IValueProviderNode
	{
		object NodeValue { get; }
	}
}

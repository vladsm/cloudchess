﻿using System;
using System.Runtime.Serialization;

namespace CloudChess.Analysis.Uci
{
	[Serializable]
	public class UciResponseParsingException : Exception
	{
		public UciResponseParsingException()
		{
		}

		public UciResponseParsingException(string message)
			: base(message)
		{
		}

		public UciResponseParsingException(string message, Exception inner)
			: base(message, inner)
		{
		}

		protected UciResponseParsingException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}
}

﻿using System;

namespace CloudChess.Analysis.Uci
{
	internal sealed class UciOkResponse : UciResponse, IEquatable<UciOkResponse>
	{
		public bool Equals(UciOkResponse other)
		{
			if (ReferenceEquals(null, other)) return false;
			return true;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			var typedObj = obj as UciOkResponse;
			return typedObj != null && Equals(typedObj);
		}

		public override int GetHashCode()
		{
			return "uciok".GetHashCode();
		}
	}
}

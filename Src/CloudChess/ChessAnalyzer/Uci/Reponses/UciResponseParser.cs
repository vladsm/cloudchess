﻿using CloudChess.Analysis.Uci.Parsing;

using Irony.Parsing;

namespace CloudChess.Analysis.Uci
{
	public interface IUciResponseParser
	{
		UciResponse Parse(string responseLine);
	}

	internal sealed class UciResponseParser<TUciResponseGrammar> : IUciResponseParser
		where TUciResponseGrammar : UciResponseGrammar, new()
	{
		private readonly Parser _internalParser;

		public UciResponseParser()
		{
			var responseGrammar = new TUciResponseGrammar();
			_internalParser = new Parser(responseGrammar);
		}

		public UciResponse Parse(string responseLine)
		{
			var parseTree = _internalParser.Parse(responseLine);

			IUciResponseNode responseNode;
			if (parseTree.HasErrors() || (responseNode = parseTree.Root.AstNode as IUciResponseNode) == null)
			{
				throw new UciResponseParsingException("Response string \"" + responseLine + "\" cannot be parsed.");
			}
			return responseNode.GetUciResponse();
		}
	}
}

﻿using System;

namespace CloudChess.Analysis.Uci
{
	internal sealed class ReadyOkUciResponse : UciResponse, IEquatable<ReadyOkUciResponse>
	{
		public bool Equals(ReadyOkUciResponse other)
		{
			if (ReferenceEquals(null, other)) return false;
			return true;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			var typedObj = obj as ReadyOkUciResponse;
			return typedObj != null && Equals(typedObj);
		}

		public override int GetHashCode()
		{
			return "readyok".GetHashCode();
		}
	}
}

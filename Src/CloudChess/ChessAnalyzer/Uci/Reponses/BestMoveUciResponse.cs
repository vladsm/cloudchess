﻿using System;

namespace CloudChess.Analysis.Uci
{
	internal sealed class BestMoveUciResponse : UciResponse, IEquatable<BestMoveUciResponse>
	{
		public CnMove BestMove { get; private set; }
		public CnMove Ponder { get; private set; }

		public BestMoveUciResponse(CnMove bestMove, CnMove ponder)
		{
			if (bestMove == null) throw new ArgumentNullException("bestMove");

			BestMove = bestMove;
			Ponder = ponder;
		}

		public BestMoveUciResponse(CnMove bestMove) : this(bestMove, null)
		{
		}

		#region Equality overridings

		public bool Equals(BestMoveUciResponse other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return Equals(BestMove, other.BestMove) && Equals(Ponder, other.Ponder);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			var typedObj = obj as BestMoveUciResponse;
			return typedObj != null && Equals(typedObj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return (BestMove.GetHashCode()*397) ^ (Ponder != null ? Ponder.GetHashCode() : 0);
			}
		}

		#endregion
	}
}

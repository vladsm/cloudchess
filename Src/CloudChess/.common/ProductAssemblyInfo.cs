﻿using System.Reflection;

[assembly: AssemblyCompany("Vladimir Smirnov")]
[assembly: AssemblyProduct("CloudChess")]
[assembly: AssemblyCopyright("Copyright © Vladimir Smirnov, 2013")]
[assembly: AssemblyTrademark("CloudChess")]

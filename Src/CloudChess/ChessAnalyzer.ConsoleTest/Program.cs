﻿using System;

namespace CloudChess.Analysis.ConsoleTest
{
	class Program
	{
		static void Main()
		{
			new AnalyzerAnalyzeTest().Test();

			Console.ReadLine();
		}
	}
}

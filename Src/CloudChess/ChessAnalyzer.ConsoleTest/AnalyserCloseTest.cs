﻿using System;
using System.Reactive.Linq;

namespace CloudChess.Analysis.ConsoleTest
{
	public class AnalyserCloseTest
	{
		public async void Test()
		{
			var analyzer = new ChessAnalyzer(@"C:\Program Files\ChessBase\Engines\Houdini 3\Houdini 3 Pro x64.exe");
			Console.WriteLine("Run analyzer");
			await analyzer.Run();

			Console.WriteLine("Is ready?");
			await analyzer.IsReady().Do(_ => Console.WriteLine("Ready OK"));

			Console.WriteLine("Close analyzer");
			//analyser.Dispose();
			await analyzer.Close();
			Console.WriteLine("Analyser is closed");
		}
	}
}

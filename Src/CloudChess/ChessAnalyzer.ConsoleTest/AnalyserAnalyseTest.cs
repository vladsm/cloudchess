﻿using System;
using System.Collections.Generic;
using System.Reactive.Linq;

using CloudChess.Analysis.Houdini;
using CloudChess.Analysis.Uci;

namespace CloudChess.Analysis.ConsoleTest
{
	public class AnalyzerAnalyzeTest
	{
		public async void Test()
		{
			var analyzer = new HoudiniChessAnalyzer(@"C:\Program Files\ChessBase\Engines\Houdini 3\Houdini 3 Pro x64.exe");
			Console.WriteLine("Run analyzer");
			await analyzer.Run();

			Console.WriteLine("Is ready?");
			await analyzer.IsReady().Do(_ => Console.WriteLine("Ready OK"));

			Console.WriteLine("Setup position");
			analyzer.SetPosition(new StartUciPosition());
			
			Console.WriteLine("Analyse!");
			var analysisTraces = new List<AnalysisTrace>();
			//var analysis = analyzer.Analyze(new PositionAnalysisParameters {TimeRestrictions = new ClockTime(60000, 60000)});
			var analysis = analyzer.Analyze(new PositionAnalysisParameters {TimeRestrictions = new ExactAnalysisTime(30000)});
			analysis.Subscribe(analysisTraces.Add);
			await analysis;

			Console.WriteLine("Analysis completed");
		}
	}
}
